package com.medymatch.handover.functional_interfaces;

public interface IConverter<FromType, ToType> {
	public ToType convertTo(FromType fromType);
}
