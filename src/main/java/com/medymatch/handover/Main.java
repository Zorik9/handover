package com.medymatch.handover;

import com.medymatch.handover.main_args.MainArguments;
import com.medymatch.handover.main_args.MainArgumentsManager;

public class Main {
	public static void main(String[] args){
		System.out.println("Running Handover Tool...");
		MainArgumentsManager mainArgumentsManager = new MainArgumentsManager();
		MainArguments mainArguments = mainArgumentsManager.getMainArguments(args);
		mainArgumentsManager.handleMainArguments(mainArguments);
	}
}