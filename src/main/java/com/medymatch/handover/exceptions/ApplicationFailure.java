package com.medymatch.handover.exceptions;

/**
 * Created by Omri at 25/10/2016 06:06
 */
public class ApplicationFailure extends RuntimeException {
  public ApplicationFailure(String msg) { super(msg); }
  public ApplicationFailure(String msg, Throwable e) { super(msg, e); }
}
