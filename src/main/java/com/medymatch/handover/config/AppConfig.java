package com.medymatch.handover.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = "com.medymatch.handover")
@Configuration
public class AppConfig {

}
