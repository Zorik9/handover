package com.medymatch.handover.config;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;

public class LoggerLoader {

	public void loadLogger(String loggerResourcePath, String loggerName){
		Properties props = new Properties();
		try {
			props.load(getClass().getResourceAsStream(loggerResourcePath));
		} catch (IOException e) {
			System.out.println("Failed to load the logger " + loggerName + "\n\n");
			e.printStackTrace();
		}
		PropertyConfigurator.configure(props);
	}
}
