package com.medymatch.handover.config;

import lombok.Getter;

public class DicomConfig {
	@Getter
	private boolean reanalyze = false;
}
