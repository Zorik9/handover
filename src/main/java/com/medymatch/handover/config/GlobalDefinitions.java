package com.medymatch.handover.config;

import java.io.File;

import lombok.Getter;

public class GlobalDefinitions {
	
	@Getter
	private boolean debugMode = true;
	
	@Getter
	private String tempFolder = "C:" + File.separator + "Temp" + File.separator +  "dicom";
	
	@Getter
	private String accessionNumberPattern = "^[+-]?\\d+$";
	
	@Getter
	private String tagNameOpeningBrackets = "(";
	
	@Getter
	private String tagNameClosingBrackets = ")";
	
	@Getter
	private String accessionNumberTagKey = "0008,0050";
	
	@Getter
	private String accessionNumberTagName = "AccessionNumber" + tagNameOpeningBrackets + accessionNumberTagKey + tagNameClosingBrackets;
	
	@Getter
	private String defaultAppPropertiesFile = "app.properties";
	
	@Getter
	private String defaultTagsPropertiesFile = "tags.properties";
	
	@Getter
	private String xlsBackupPostfix = "_bck";
	
	@Getter
	private String dicomFileExtenssion = ".dcm";
	
	@Getter
	private String textFileExtenssion = ".txt";
}
