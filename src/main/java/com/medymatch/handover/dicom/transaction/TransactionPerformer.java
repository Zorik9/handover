package com.medymatch.handover.dicom.transaction;

import java.io.File;
import java.util.List;

import com.medymatch.handover.dicom.output.ResultsCopier;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Service;

import com.medymatch.handover.data.app.HandoverProperties;
import com.medymatch.handover.data.app.TransactionInput;
import com.medymatch.handover.main_args.MainArguments;
import com.medymatch.handover.utils.file.FilePathBuilder;
import com.medymatch.handover.utils.file.IFilePathBuilder;

@Service("TransactionPerformerService")
@Scope("prototype")
public class TransactionPerformer implements ITransactionPerformer {
	
	private static Logger log = Logger.getLogger(TransactionPerformer.class);
			
	private AbstractApplicationContext context;
	private HandoverProperties handoverProperties;
	private MainArguments mainArguments;
	private File tempDir;
	
	private ITransactionExecutor transactionExecutor;
	private ResultsCopier resultsCopier;
	private ITransactionDataPreparer transactionDataPreparer;
	
	@Autowired
	private IFilePathBuilder filePathBuilder;
	
	public TransactionPerformer(MainArguments mainArguments, AbstractApplicationContext context, File tempDir){
		this.mainArguments = mainArguments;
		this.context = context;
		this.tempDir = tempDir;
		
		filePathBuilder = new FilePathBuilder();
	}
	
	public void performTransaction(){
		preTransactionSteps();
		List<String> analyzedDirs = executeTransaction();
		resultsCopier.copyDicomResultsToOutputDir(analyzedDirs);
	}
	
	private void preTransactionSteps(){
		TransactionInput transactionInput = getTransactionInput();
		validateTransactionInput(transactionInput);
		
		String xlsFile = transactionInput.getXlsFile();
		initTransactionResultsCopier(xlsFile, filePathBuilder.getFileName(transactionInput.getWorkingDirDicomRootPath()));
		initTransactionExecutor(transactionInput);	
	}

	private List<String> executeTransaction() {
		log.info("Executing anonymization transaction...\n");
		System.out.println("Analyzing case studies...\n");
		
		List<String> analyzedDirs = transactionExecutor.executeTransaction();
		return analyzedDirs;
	}

	private void validateTransactionInput(TransactionInput transactionInput) {
		if(transactionInput.getXlsFile() == null){
			throw new RuntimeException("Missing xls file in transaction input");
		}
		
		if(transactionInput.getTagsPropertiesFile() == null){
			throw new RuntimeException("Missing tags properties file in transaction input");
		}
	}
	
	private TransactionInput getTransactionInput(){
		initTransactionDataPreparer();
		TransactionInput transactionInput = transactionDataPreparer.prepareDataForTransaction();
		handoverProperties = transactionInput.getHandoverProperties();
		return transactionInput;
	}
	
	private void initTransactionDataPreparer(){
		log.info("Creating an instance of transaction data preparer service.");
		transactionDataPreparer = (ITransactionDataPreparer)context.getBean("TransactionDataPreparerService", mainArguments, tempDir, handoverProperties);
	}

	private void initTransactionExecutor(TransactionInput transactionInput) {
		log.info("Creating an instance of transaction executor service.");
		transactionExecutor = (ITransactionExecutor)context.getBean("TransactionExecutorService", transactionInput.getWorkingDirXlsFile(), handoverProperties.getDicomRootFolder(), transactionInput.getTagsPropertiesFile(), handoverProperties, context);
	}
	
	private void initTransactionResultsCopier(String xlsFile, String dicomFolderName){
		log.info("Creating an instance of transaction results copier service.");
		resultsCopier = (ResultsCopier)context.getBean("ResultsCopierService", xlsFile, dicomFolderName, handoverProperties.getOutputDir(), tempDir);
	}
}
