package com.medymatch.handover.dicom.transaction;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Service;

import com.medymatch.handover.data.analysis.SheetAnalysisResult;
import com.medymatch.handover.data.app.HandoverProperties;
import com.medymatch.handover.dicom.analyzers.sheet.IExcelSheetAnalyzer;
import com.medymatch.handover.dicom.analyzers.sheet.ISheetAnalyzerFactory;
import com.medymatch.handover.dicom.analyzers.sheet.ISheetStatisticsDisplayer;
import com.medymatch.handover.input.excel.ExcelWorkbookReader;
import com.medymatch.handover.utils.excel.IWorkbookUtils;

@Service("TransactionExecutorService")
@Scope("prototype")
public class TransactionExecutor implements ITransactionExecutor{
	
	private static Logger log = Logger.getLogger(TransactionExecutor.class);
	
	private AbstractApplicationContext context;
	
	private String xlsFilePath;
	private String tagsPropertiesFile;
	private HandoverProperties handoverProperties;
	private String inputDir;

	@Autowired
	private IWorkbookUtils workbookUtils;
	
	public TransactionExecutor(String xlsFilePath, String inputDir, String tagsPropertiesFile, HandoverProperties handoverProperties, AbstractApplicationContext context){
		this.xlsFilePath = xlsFilePath;
		this.tagsPropertiesFile = tagsPropertiesFile;
		this.handoverProperties = handoverProperties;
		this.inputDir = inputDir;
		this.context = context;
	}

	public List<String> executeTransaction(){
		
		Workbook workbook = getWorkbook(xlsFilePath);
		
		Sheet firstSheet = workbook.getSheetAt(0);
		
		IExcelSheetAnalyzer excelSheetAnalyzer = getSheetAnalyzer(firstSheet);
		
		SheetAnalysisResult sheetAnalysisResult = excelSheetAnalyzer.analyzeSheet();
		
		List<String> analyzedDirs = sheetAnalysisResult.getAnalyzedDirs();
		
		updateExcelFile(workbook);

		ISheetStatisticsDisplayer sheetStatisticsDisplayer = getSheetStatisticsDisplayer(sheetAnalysisResult);
		
		notifySuccessMessage(sheetStatisticsDisplayer.getStatistics());
		
		return analyzedDirs;
	}

	private void updateExcelFile(Workbook workbook) {
		workbookUtils.writeToWorkbook(xlsFilePath, workbook);
		workbookUtils.closeWorkbook(workbook);
	}

	private void notifySuccessMessage(String statistics) {
		System.out.println("\n\nExcel file was successfully processed.");
		System.out.println(statistics);
	}
	
	private Workbook getWorkbook(String xlsFilePath) {
		log.info("Creating an instance of excel workbook reader service.");
		ExcelWorkbookReader workbookReader = (ExcelWorkbookReader)context.getBean("ExcelWorkbookReaderService", xlsFilePath);
		return workbookReader.readWorkbook();
	}
	
	private IExcelSheetAnalyzer getSheetAnalyzer(Sheet sheet){
		log.info("Creating an instance of sheet analyzer factory service.");
		ISheetAnalyzerFactory sheetAnalyzerFactory = (ISheetAnalyzerFactory)context.getBean("SheetAnalyzerFactoryService", context, handoverProperties, inputDir, sheet, tagsPropertiesFile);
		return sheetAnalyzerFactory.getSheetAnalyzer();
	}
	
	private ISheetStatisticsDisplayer getSheetStatisticsDisplayer(SheetAnalysisResult sheetAnalysisResult){
		log.info("Creating an instance of sheet statistics displayer service.");
		return (ISheetStatisticsDisplayer)context.getBean("SheetStatisticsDisplayerService", sheetAnalysisResult);
	}
}
