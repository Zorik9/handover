package com.medymatch.handover.dicom.transaction;

import java.io.File;
import java.nio.file.Paths;

import com.medymatch.handover.exceptions.ApplicationFailure;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.medymatch.handover.config.AppConfig;
import com.medymatch.handover.config.GlobalDefinitions;
import com.medymatch.handover.main_args.MainArguments;
import com.medymatch.handover.utils.file.IDirectoryDeleter;

@Service
@ContextConfiguration(classes = AppConfig.class, loader = AnnotationConfigContextLoader.class)
public class TransactionManager {
	private static Logger log = Logger.getLogger(TransactionManager.class);
			
	private MainArguments mainArguments;
	
	private AbstractApplicationContext context;
	private IDirectoryDeleter directoryDeleter;
	private File tempDir;
	private GlobalDefinitions globalDefinitions;
	
	private ITransactionPerformer transactionPerformer;
	
	public TransactionManager(MainArguments mainArguments) {
		this.mainArguments = mainArguments;
		globalDefinitions = new GlobalDefinitions();
	}
	
	public boolean doTransaction(){
		try{
			if(!prepareEnvForTransaction()){
				return false;
			}
			
			transactionPerformer.performTransaction();
			return finalizeTransaction();
		} catch (ApplicationFailure af) {
			System.err.println(af.getMessage());
			log.error("Application failure",af);
			return false;
		} catch(Throwable t){
			log.error("The transaction failed.\n\n", t);

			if(globalDefinitions.isDebugMode()){
				t.printStackTrace();
			}
			return false;
		}
	}
	
	private boolean prepareEnvForTransaction(){
		initTransaction();
		return deleteTempDir();
	}
	
	private void initTransaction(){
		log.info("Initializing transaction components.");
		initContex();
		initDirectoryDeleter();
		initTempDir();
		initTransactionPerformer();
	}
	
	private void initContex(){
		log.info("Creating a context instance for Handover project.");
		context = new AnnotationConfigApplicationContext(AppConfig.class);
	}
	
	private void initTransactionPerformer(){
		log.info("Creating an instance of transaction performer service.");
		transactionPerformer = (ITransactionPerformer)context.getBean("TransactionPerformerService", mainArguments, context, tempDir);
	}
	
	private void initDirectoryDeleter(){
		log.info("Creating an instance of directory deleter service.");
		directoryDeleter = (IDirectoryDeleter)context.getBean("DirectoryDeleterService");
	}
	
	private void initTempDir(){
		log.info("Initializing temp dir...");
		GlobalDefinitions globalDefinitions = new GlobalDefinitions();
		
		log.info("Creating temp dir File object.");
		tempDir = Paths.get(globalDefinitions.getTempFolder()).toFile();
		
		if(tempDir == null){
			throw new RuntimeException("Failed to create temp dir.");
		}
		log.info("Temp dir path is: " + tempDir.getPath());
		
		log.info("Creating temp dir...");
		tempDir.mkdirs();
		
		log.info("Setting temp dir to be writable...");
		tempDir.setWritable(true);
		log.info("Temp dir has been successfully initialized.");
	}
	
	private boolean finalizeTransaction(){
		return closeContext() && deleteTempDir();
	}
	
	private boolean closeContext(){
		if(context != null){
			context.close();
			log.info("Spring context has been successfully closed.");
			return true;
		}
		else{
			log.warn("Failed to close spring context, because it's already empty.");
			return false;
		}
	}
	
	private boolean deleteTempDir(){
		log.info("Deleting temp directory...");
		
		if(directoryDeleter.deleteDirectory(tempDir)){
			log.info("Temp directory has been successfully deleted.");
			return true;
		}
		else{
			log.warn("Failed to delete temp dir.");
			return false;
		}
	}
}
