package com.medymatch.handover.dicom.transaction;

public interface ITransactionPerformer {
	public void performTransaction();
}
