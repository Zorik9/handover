package com.medymatch.handover.dicom.transaction;

import com.medymatch.handover.data.app.TransactionInput;

public interface ITransactionDataPreparer {
	public TransactionInput prepareDataForTransaction();
}
