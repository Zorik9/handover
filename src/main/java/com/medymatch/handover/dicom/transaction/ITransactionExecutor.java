package com.medymatch.handover.dicom.transaction;

import java.util.List;

public interface ITransactionExecutor {
	/** Retrieves list of analyzed directories. */
	public List<String> executeTransaction();
}
