package com.medymatch.handover.dicom.transaction;

import java.io.File;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.medymatch.handover.config.GlobalDefinitions;
import com.medymatch.handover.data.app.HandoverProperties;
import com.medymatch.handover.data.app.TransactionInput;
import com.medymatch.handover.input.properties.IHandoverPropertiesLoader;
import com.medymatch.handover.input.properties.IPropertiesLoader;
import com.medymatch.handover.main_args.MainArguments;
import com.medymatch.handover.utils.file.IConfigFileFetcher;
import com.medymatch.handover.utils.file.IDirectoryCopier;
import com.medymatch.handover.utils.file.IFilePathBuilder;

@Service("TransactionDataPreparerService")
@Scope("prototype")
public class TransactionDataPreparer implements ITransactionDataPreparer {
	
	private static Logger log = Logger.getLogger(TransactionDataPreparer.class);
	
	@Autowired
	private IConfigFileFetcher configFileFetcher;
	
	@Autowired
	private IHandoverPropertiesLoader handoverPropertiesLoader;
	
	@Autowired
	private IPropertiesLoader propertiesLoader;
	
	@Autowired
	private IDirectoryCopier directoryCopier;
	
	private MainArguments mainArguments;
	private File tempDir;
	private HandoverProperties handoverProperties;
	
	private GlobalDefinitions globalDefinitions;
	
	@Autowired
	private IFilePathBuilder filePathBuilder;
	
	public TransactionDataPreparer(MainArguments mainArguments, File tempDir, HandoverProperties handoverProperties){
		this.mainArguments = mainArguments;
		this.tempDir = tempDir;
		this.handoverProperties = handoverProperties;
		globalDefinitions = new GlobalDefinitions();
	}
	
	public TransactionInput prepareDataForTransaction(){
		log.info("Fetching tag properties file path.");
		String tagsPropertiesFile = configFileFetcher.fetchActualConfigFilePath(mainArguments.getTagsPropertiesFile(),
														globalDefinitions.getDefaultTagsPropertiesFile());
		
		if(tagsPropertiesFile == null){
			throw new RuntimeException("Failed to load tag properties file.");
		}
		
		log.info("Tags properties file has been successfully loaded.");
		
		log.debug("Fetching app properties pbject.");
		Properties appProperties = getAppProperties();
		
		log.debug("Convering app properties object to Handover properties object.");
		handoverProperties = handoverPropertiesLoader.getHandoverProperties(appProperties);

		String outputDir = tempDir.getAbsolutePath();
		
		String infoMessage = String.format("Copy excel file to the temporary directory: %s ...", outputDir);
		System.out.println(infoMessage);
		log.info(infoMessage);
		directoryCopier.copyFileToDir(handoverProperties.getXlsFile(), outputDir);
		
		infoMessage = "Temp directory data is ready for analysis\n";
		System.out.println(infoMessage);
		log.info(infoMessage);
		return getTransactionInput(tagsPropertiesFile);
	}
	
	private TransactionInput getTransactionInput(String tagsPropertiesFile){
		log.debug("Creating transaction input.");
		
		TransactionInput transactionInput = new TransactionInput();
		
		transactionInput.setDicomRootFolder(handoverProperties.getDicomRootFolder());
		String xlsFileName = filePathBuilder.getFileName(handoverProperties.getXlsFile());
		String workingDirXlsFile = filePathBuilder.getFilePath(tempDir, xlsFileName);
		transactionInput.setWorkingDirXlsFile(workingDirXlsFile);
		
		String dicomRootFolderName = filePathBuilder.getFileName(handoverProperties.getDicomRootFolder());
		String workingDirDicomRootPath = filePathBuilder.getFilePath(tempDir, dicomRootFolderName);
		transactionInput.setWorkingDirDicomRootPath(workingDirDicomRootPath);
		
		transactionInput.setTempDir(tempDir);
		transactionInput.setXlsFile(handoverProperties.getXlsFile());
		transactionInput.setTagsPropertiesFile(tagsPropertiesFile);
		transactionInput.setHandoverProperties(handoverProperties);
		return transactionInput;
	}
	
	private Properties getAppProperties() {
		log.info("Fetching app properties file path.");
		String appPropertiesFile = configFileFetcher.fetchActualConfigFilePath(mainArguments.getAppPropertiesFile(),
														globalDefinitions.getDefaultAppPropertiesFile());
		
		/*
		 * Note that: if appPropertiesFile is empty, then
		 * We will try to fetch it from the resource folder.
		 */
		
		log.debug("Extracting properties object from app properties file path.");
		Properties appProperties = getAppProperties(appPropertiesFile);
		return appProperties;
	}
	
	private Properties getAppProperties(String appPropertiesFile) {
		
		Properties appProperties = StringUtils.isEmpty(appPropertiesFile)?
									propertiesLoader.readPropertiesFromResourceFolder(appPropertiesFile) :
									propertiesLoader.readPropertiesFromFile(appPropertiesFile);
									
		if(appProperties == null){
			appPropertiesFile = globalDefinitions.getDefaultAppPropertiesFile();
			appProperties = propertiesLoader.readPropertiesFromFile(appPropertiesFile);
		}
		
		if(appProperties == null){
			throw new RuntimeException("App properties file is missing.\nThis configuration file is mandatory.\nPlease supply the properties file to the program input.");
		}
		return appProperties;
	}
}
