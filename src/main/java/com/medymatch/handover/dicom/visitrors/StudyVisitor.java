package com.medymatch.handover.dicom.visitrors;

import com.medymatch.handover.data.analysis.DicomParams;
import com.medymatch.handover.dicom.ITagReplacementAlgorithm;
import com.medymatch.handover.dicom.analyzers.study.StudyCaseAnalyzer;
import lombok.Getter;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import pl.psnc.scape.dicom.config.AppConfig;
import pl.psnc.scape.dicom.data.DirAnonymizationResults;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public class StudyVisitor {
	
	@Getter
	private DirAnonymizationResults dirAnonymizationResults;
	
	@Getter
	private Map<Integer, String> tagsReplacement;
	
	@Getter
	private List<String> analyzedDirs;
	
	private String accessionNumber;
	
	private ITagReplacementAlgorithm tagReplacementAlgorithm;
	
	private AbstractApplicationContext padiMasterContext;
	
	private StudyCaseAnalyzer studyCaseAnalyzer;
	
	public StudyVisitor(String accessionNumber, ITagReplacementAlgorithm tagReplacementAlgorithm){
		this.accessionNumber = accessionNumber;
		this.tagReplacementAlgorithm = tagReplacementAlgorithm;
	}

	public FileVisitResult visitStudy(Path studyDirectory, String outputDirectory) throws IOException {
		initStudyCaseAnalyzer();
		File currentStudy = studyDirectory.toFile();
		
		dirAnonymizationResults = analyzeStudyCase(currentStudy, outputDirectory);
		analyzedDirs = studyCaseAnalyzer.getAnalyzedDirs();
		return continueVisitFile();
	}
	
	private DirAnonymizationResults analyzeStudyCase(File currentStudy, String outputDirectory){
		DicomParams dicomParams =  tagReplacementAlgorithm.applyPolicy();
		tagsReplacement = dicomParams.getTagsReplacement();
		File[] dcmFiles = currentStudy.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".dcm");
			}
		});
		return studyCaseAnalyzer.analyzeStudyCase(dicomParams, dcmFiles, outputDirectory);
	}
	
	private FileVisitResult continueVisitFile(){
		padiMasterContext.close();
		return FileVisitResult.CONTINUE;	
	}
	
	private void initStudyCaseAnalyzer(){
		padiMasterContext = new AnnotationConfigApplicationContext(AppConfig.class);
		studyCaseAnalyzer = new StudyCaseAnalyzer(padiMasterContext, accessionNumber);
	}
}
