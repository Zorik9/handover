package com.medymatch.handover.dicom.visitrors;

import lombok.extern.java.Log;
import org.dcm4che2.data.DicomObject;
import pl.psnc.scape.dicom.io.reader.DicomReader;
import pl.psnc.scape.dicom.io.reader.IDicomReader;
import pl.psnc.scape.dicom.tag.TagFetcher;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

@Log
public class AccessionNumber2Directory implements FileVisitor<Path> {
  private Map<String, Path> accession2Dir = new HashMap<>();
  private IDicomReader dicomReader;
  private int accessionNumberOffset;


  public AccessionNumber2Directory(){
    TagFetcher tagFetcher = new TagFetcher();
    accessionNumberOffset = tagFetcher.getTag("(0008,0050)");

    dicomReader = new DicomReader();
  }

  @Override
  public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
    String accessionNumber = getAccession(dir);
    if (accessionNumber!=null) {
      accession2Dir.put(accessionNumber,dir);
      return FileVisitResult.SKIP_SUBTREE;
    }
    return FileVisitResult.CONTINUE;
  }

  private String getAccession(Path dir) {
    File[] dicoms = dir.toFile().listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return (!new File(dir,name).isDirectory()) && name.endsWith(".dcm");
      }
    });
    boolean isStudy = dicoms.length>0;

    String ret = null;
    if (isStudy) {
      DicomObject dicom = dicomReader.read(dicoms[0].getAbsolutePath());
      ret = dicom.getString(accessionNumberOffset);
    }

    return ret;
  }

  @Override
  public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
    return FileVisitResult.CONTINUE;
  }

  @Override
  public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
    return FileVisitResult.CONTINUE;
  }

  public void loadStudies(Path parentDirectory) {
    try {
      Files.walkFileTree(parentDirectory,this);
    } catch (IOException e) {
      log.log(Level.WARNING,"failed to analyze study directory",e);
    }
  }

  public Path getDirectory(String accessionNumber) {
    return (accessionNumber!=null) ?
        accession2Dir.get(accessionNumber) :
        null;
  }
}
