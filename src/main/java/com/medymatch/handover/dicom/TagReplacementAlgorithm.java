package com.medymatch.handover.dicom;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.medymatch.handover.data.analysis.DicomParams;
import com.medymatch.handover.data.app.TagReplacementPolicy;

import pl.psnc.scape.dicom.tag.TagFetcher;

@Service("TagReplacementAlgorithmService")
@Scope("prototype")
public class TagReplacementAlgorithm implements ITagReplacementAlgorithm{
	private TagReplacementPolicy policy;
	private String tagsPropertiesFile;
	
	public TagReplacementAlgorithm(TagReplacementPolicy policy, String tagsPropertiesFile){
		this.policy = policy;
		this.tagsPropertiesFile = tagsPropertiesFile;
	}
	
	public DicomParams applyPolicy(){
		return getDicomParams(getTagsReplacement());
	}

	private Map<Integer, String> getTagsReplacement() {
		Map<Integer, String> tagsReplacement = new HashMap<>();
		
		for (String tagKey : policy.getTagReplacementKeys()) {
			int tagId = new TagFetcher().getTag(tagKey);
			String tagReplacementValue = getTagReplacementValue();
			tagsReplacement.put(tagId, tagReplacementValue);
		}
		return tagsReplacement;
	}
	
	private String getTagReplacementValue(){
		return policy.getPrefix() + String.valueOf(getNowTime() % (long)Math.pow(10, policy.getNumOfWantedDigits()));
	}

	private long getNowTime() {
		long nowTime = policy.getNowTime();
		
		if(nowTime < 0){
			nowTime = new Date().getTime();
		}
		return nowTime;
	}
	
	private DicomParams getDicomParams(Map<Integer, String> tagsReplacement) {
		DicomParams dicomParams = new DicomParams();
		dicomParams.setConfigFilePath(tagsPropertiesFile);
		dicomParams.setTagsReplacement(tagsReplacement);
		return dicomParams;
	}
}
