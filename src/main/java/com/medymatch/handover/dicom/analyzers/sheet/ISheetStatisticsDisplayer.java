package com.medymatch.handover.dicom.analyzers.sheet;

public interface ISheetStatisticsDisplayer {
	public String getStatistics();
}
