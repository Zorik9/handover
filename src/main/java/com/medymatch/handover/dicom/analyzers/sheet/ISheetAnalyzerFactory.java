package com.medymatch.handover.dicom.analyzers.sheet;

import com.medymatch.handover.dicom.analyzers.sheet.IExcelSheetAnalyzer;

public interface ISheetAnalyzerFactory {
	public IExcelSheetAnalyzer getSheetAnalyzer();
}
