package com.medymatch.handover.dicom.analyzers.sheet;

import java.io.File;
import java.util.List;

import com.medymatch.handover.dicom.visitrors.AccessionNumber2Directory;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.medymatch.handover.data.analysis.ExcelSheetAnalyzerParams;
import com.medymatch.handover.data.analysis.SheetAnalysisResult;
import com.medymatch.handover.utils.excel.IColumnUtils;
import com.medymatch.handover.utils.excel.IRowUtils;

@Service("ExcelSheetAnalyzerService")
@Scope("prototype")
public class ExcelSheetAnalyzer implements IExcelSheetAnalyzer{
	
	private static Logger log = Logger.getLogger(ExcelSheetAnalyzer.class);
	
	private ExcelSheetAnalyzerParams excelSheetAnalyzerParams;
	
	@Autowired
	private IRowUtils rowUtils;
	
	@Autowired
	private IColumnUtils columnUtils;

	private AccessionNumber2Directory accessionNumber2Directory;
	
	public ExcelSheetAnalyzer(ExcelSheetAnalyzerParams excelSheetAnalyzerParams){
		this.excelSheetAnalyzerParams = excelSheetAnalyzerParams;
	}
	
	public SheetAnalysisResult analyzeSheet(){
		excelSheetAnalyzerParams.getHeadersUpdater().updateHeaders();
		SheetAnalysisResult sheetAnalysisResult = analyzeExcelRows();
		excelSheetAnalyzerParams.getColumnsAutoSizer().autoSizeColumns();
		return sheetAnalysisResult;
	}

	private SheetAnalysisResult analyzeExcelRows() {
		accessionNumber2Directory = new AccessionNumber2Directory();
		accessionNumber2Directory.loadStudies(new File(excelSheetAnalyzerParams.getHandoverProperties().getDicomRootFolder()).toPath());
		excelSheetAnalyzerParams.getExcelRowAnalyzer().setAccessionNumber2Directory(accessionNumber2Directory);

		SheetAnalysisResult sheetAnalysisResult = new SheetAnalysisResult();
		int numOfRows = excelSheetAnalyzerParams.getSheet().getPhysicalNumberOfRows();
		Row row;
		
		for (int i = excelSheetAnalyzerParams.getHandoverProperties().getFirstDataRow(); i <= numOfRows; i++) {
			row = excelSheetAnalyzerParams.getSheet().getRow(i);
			
			if(isEmptyRow(row, i)){
				notifyRowSkipping(i);
				continue;
			}
			
			sheetAnalysisResult.addRowAnalysisResult(excelSheetAnalyzerParams.getExcelRowAnalyzer().analyzeRow(row));
		}
		
		sheetAnalysisResult.setAnalyzedDirs(excelSheetAnalyzerParams.getExcelRowAnalyzer().getAnalyzedDirs());
		return sheetAnalysisResult;
	}

	private void notifyRowSkipping(int rowNumber) {
		String infoMessage;
		infoMessage = "Skipping row # " + rowNumber + " because it's empty.";
		log.info(infoMessage);
		System.out.println(infoMessage);
	}
	
	private boolean isEmptyRow(Row row, int rowIndex){
		List<Integer> columns = columnUtils.extractColumns(excelSheetAnalyzerParams.getHandoverProperties());
		return rowUtils.isEmptyRow(row, columns);
	}
}
