package com.medymatch.handover.dicom.analyzers.row;

import java.util.Map;

import lombok.Data;
import pl.psnc.scape.dicom.tag.ITagFetcher;

@Data
public abstract class AbstractRowCustomCellsUpdater {
	protected ITagFetcher tagFetcher;
	
	protected Map<String, String> customColumnMap;
}
