package com.medymatch.handover.dicom.analyzers.sheet;

import com.medymatch.handover.data.analysis.SheetAnalysisResult;

public interface IExcelSheetAnalyzer {
	public SheetAnalysisResult analyzeSheet();
}
