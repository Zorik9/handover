package com.medymatch.handover.dicom.analyzers.sheet;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.medymatch.handover.config.DicomConfig;
import com.medymatch.handover.data.analysis.SheetAnalysisResult;

@Service("SheetStatisticsDisplayerService")
@Scope("prototype")
public class SheetStatisticsDisplayer implements ISheetStatisticsDisplayer {
	private SheetAnalysisResult sheetAnalysisResult;
	
	public SheetStatisticsDisplayer(SheetAnalysisResult sheetAnalysisResult){
		this.sheetAnalysisResult = sheetAnalysisResult;
	}
	
	public String getStatistics(){
		StringBuilder statisticsMessage = new StringBuilder();
		statisticsMessage.append(getStatisticsTitle())
						 .append(getAnalyzedRowsStatistic())
						 .append(getAlreadyAnalyzedRowsStatisticIfNeeded())
						 .append(getInvalidRowsStatistic())
						 .append(getEmptyRowsStatistic())
						 .append("Total number of rows: " + sheetAnalysisResult.getTotalNumOfRows() + "\n");
		
		return statisticsMessage.toString();
	}
	
	private StringBuilder getStatisticsTitle(){
		return new StringBuilder().append("\n").append("***********")
				.append("\nStatistics:\n")
				.append("***********").append("\n");
	}
	
	private StringBuilder getAlreadyAnalyzedRowsStatisticIfNeeded(){
		DicomConfig dicomConfig = new DicomConfig();
		
		if(!dicomConfig.isReanalyze()){
			return getAlreadyAnalyzedRowsStatistic();
		}
		
		return new StringBuilder();
	}
	
	private StringBuilder getAnalyzedRowsStatistic(){
		return getStatisticsSingleResult(sheetAnalysisResult.getNumOfAnalyzedRows(), "Number of analyzed rows", "All of the rows have been skipped");
	}
	
	private StringBuilder getAlreadyAnalyzedRowsStatistic(){
		return getStatisticsSingleResult(sheetAnalysisResult.getNumOfAnalyzedRows(), "Number of rows that are already analyzed from previous run", "There are no rows that are already analyzed");
	}

	private StringBuilder getInvalidRowsStatistic(){
		return getStatisticsSingleResult(sheetAnalysisResult.getNumOfInvalidRows(), "Number of invalid rows", "All of the rows contain valid input");
	}
	
	private StringBuilder getEmptyRowsStatistic(){
		return getStatisticsSingleResult(sheetAnalysisResult.getNumOfEmptyResultRows(), "Number of rows with accessesion number that did not match the DICOM files", "The accession number of each of the valid rows matched the DICOM files");
	}
	
	private StringBuilder getStatisticsSingleResult(int counter, String statisticExistMessage, String statisticNotExistMessage){
		StringBuilder msg = new StringBuilder();
		
		if(counter > 0){
			msg.append(statisticExistMessage + ": " + counter);
		}
		else{
			msg.append(statisticNotExistMessage + ".");
		}
		
		msg.append("\n\n");
		return msg;
	}
}
