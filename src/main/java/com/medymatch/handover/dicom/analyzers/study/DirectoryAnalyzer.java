package com.medymatch.handover.dicom.analyzers.study;

import com.medymatch.handover.config.GlobalDefinitions;
import com.medymatch.handover.data.analysis.DicomParams;
import com.medymatch.handover.exceptions.ApplicationFailure;
import com.medymatch.handover.utils.file.DirectoryCopier;
import com.medymatch.handover.utils.file.DirectoryDeleter;
import com.medymatch.handover.utils.file.FileMover;
import com.medymatch.handover.utils.file.FilePathBuilder;
import lombok.Getter;
import lombok.extern.java.Log;
import org.apache.commons.io.FileUtils;
import org.springframework.context.support.AbstractApplicationContext;
import pl.psnc.scape.dicom.data.DirAnonymizationResults;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Log
public class DirectoryAnalyzer {
	private AbstractApplicationContext padiMasterContext;

	private AccessionNumberValidator accessionNumberValidator;

	private StudyAnalyzer studyAnalyzer;

	private String accessionNumber;

	@Getter
	private List<String> analyzedDirs;
	
	public DirectoryAnalyzer(AbstractApplicationContext padiMasterContext, String accessionNumber){
		this.padiMasterContext = padiMasterContext;
		this.accessionNumber = accessionNumber;
		initAccessionNumberValidator();
		initSdudyAnalyzer();
	}
	
	private void initAccessionNumberValidator(){
		accessionNumberValidator = new AccessionNumberValidator(padiMasterContext);
	}
	
	private void initSdudyAnalyzer(){
		studyAnalyzer = new StudyAnalyzer(padiMasterContext);
	}

	public DirAnonymizationResults analyzeDir(DicomParams dicomParams, File[] dcmFiles, String outputDirectory){
		if(accessionNumberValidator.isMatchesAccessionNumber(dcmFiles[0].getPath(), accessionNumber)){
			String workingDirectory = copyStudyToWorkingDir(dcmFiles[0].getParentFile());
			dicomParams.setInputDirPath(workingDirectory);

			DirAnonymizationResults dirAnonymizationResults = studyAnalyzer.analyzeStudy(dicomParams);
			moveAnonymizedToOutputDir(workingDirectory,outputDirectory);
			analyzedDirs = studyAnalyzer.getAnalyzedDirs();
			return dirAnonymizationResults;
		}
		return null;
	}

	private void moveAnonymizedToOutputDir(String workingDir, String outputDir) {
		log.info("Move analysis results to from temporary directory to output directory...\n");
		try {
			File src = new File(workingDir);
			File tgt = new File(outputDir, accessionNumber);
			if (tgt.exists()) {
				log.info(String.format("replacing existing directory %s from output",src.getName()));
				new DirectoryDeleter().deleteDirectory(tgt);
			}
			FileUtils.moveDirectory(src,tgt);
		} catch (IOException e) {
			throw new ApplicationFailure("Could not copy dicoms to output dir",e);
		}
	}

	private String copyStudyToWorkingDir(File studyDir){
		FilePathBuilder filePathBuilder = new FilePathBuilder();
		GlobalDefinitions globalDefinitions = new GlobalDefinitions();
		String workingDir = filePathBuilder.getFilePath(new File(globalDefinitions.getTempFolder()), studyDir.getName());

		DirectoryCopier directoryCopier=new DirectoryCopier();
		String infoMessage = String.format("Copy dicom files to the directory: %s ...", workingDir);
		System.out.println(infoMessage);
		log.info(infoMessage);
		directoryCopier.copyDirToDir(studyDir, new File(workingDir));
		return workingDir;
	}
}
