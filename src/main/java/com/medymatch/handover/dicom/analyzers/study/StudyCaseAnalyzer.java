package com.medymatch.handover.dicom.analyzers.study;

import java.io.File;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Service;

import com.medymatch.handover.data.analysis.DicomParams;

import lombok.Getter;
import pl.psnc.scape.dicom.data.DirAnonymizationResults;

@Service("StudyCaseAnalyzerService")
@Scope("prototype")
public class StudyCaseAnalyzer  {
	
	@Getter
	private List<String> analyzedDirs;
	
	private AbstractApplicationContext padiMasterContext;
	
	private String accessionNumber;
	
	private DirectoryAnalyzer directoryAnalyzer;
	
	public StudyCaseAnalyzer(AbstractApplicationContext padiMasterContext, String accessionNumber){
		this.padiMasterContext = padiMasterContext;
		this.accessionNumber = accessionNumber;
		initDirectoriesAnalyzer();
	}
	
	public DirAnonymizationResults analyzeStudyCase(DicomParams dicomParams, File[] dcmFiles, String outputDirectory) {
		
		if(dcmFiles == null || dcmFiles.length == 0){
			return null;
		}
		DirAnonymizationResults dirAnonymizationResults;
		
		dirAnonymizationResults = directoryAnalyzer.analyzeDir(dicomParams, dcmFiles, outputDirectory);
		analyzedDirs = directoryAnalyzer.getAnalyzedDirs();

		return dirAnonymizationResults;
	}
	
	private void initDirectoriesAnalyzer(){
		directoryAnalyzer = new DirectoryAnalyzer(padiMasterContext, accessionNumber);
	}
}
