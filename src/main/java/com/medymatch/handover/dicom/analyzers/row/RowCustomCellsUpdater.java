package com.medymatch.handover.dicom.analyzers.row;

import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.dcm4che2.data.DicomObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import pl.psnc.scape.dicom.tag.ITagFetcher;

@Service("RowCustomCellsUpdaterService")
@Scope("prototype")
public class RowCustomCellsUpdater extends AbstractRowCustomCellsUpdater implements IRowCustomCellsUpdater {
	
	public RowCustomCellsUpdater(ITagFetcher tagFetcher, Map<String, String> customColumnMap) {
		this.tagFetcher = tagFetcher;
		this.customColumnMap = customColumnMap;
	}
	
	public void updateCustomCells(Row row, DicomObject dicomObject){
		int tagNumber;
		int coulumnIndex;
		Cell customCell;
		String customCellValue;
		
		for (Map.Entry<String, String> entry : customColumnMap.entrySet()) {
		    tagNumber = getTagNumber(entry);
		    coulumnIndex = getColumnIndex(entry);
		    customCell = getCustomCell(row, coulumnIndex);
		    customCellValue = tagFetcher.getTagValue(dicomObject, tagNumber);
		    
		    updateCusomCell(customCell, customCellValue);
		}
	}
	
	private int getTagNumber(Map.Entry<String, String> entry) {
		String tagName = entry.getKey();
		return tagFetcher.getTag(tagName);
	}

	private int getColumnIndex(Map.Entry<String, String> entry) {
		String columnLetter = entry.getValue();
		return CellReference.convertColStringToIndex(columnLetter);
	}

	private void updateCusomCell(Cell customCell, String customCellValue) {
		customCell.setCellType(Cell.CELL_TYPE_STRING);
		customCell.setCellValue(customCellValue);
	}

	private Cell getCustomCell(Row row, int coulumnIndex) {
		Cell customCell;
		customCell = row.getCell(coulumnIndex);
		
		if(customCell == null){
			customCell = row.createCell(coulumnIndex);
		}
		return customCell;
	}
}
