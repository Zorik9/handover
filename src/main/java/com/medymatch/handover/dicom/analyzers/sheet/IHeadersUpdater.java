package com.medymatch.handover.dicom.analyzers.sheet;

public interface IHeadersUpdater {
	public void updateHeaders();
}
