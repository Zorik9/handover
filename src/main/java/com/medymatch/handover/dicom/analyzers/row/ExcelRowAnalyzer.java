package com.medymatch.handover.dicom.analyzers.row;

import com.medymatch.handover.data.analysis.ExcelRowAnalyzerParams;
import com.medymatch.handover.data.analysis.RowAnalysisResult;
import com.medymatch.handover.data.analysis.StudyParams;
import com.medymatch.handover.dicom.visitrors.AccessionNumber2Directory;
import com.medymatch.handover.dicom.visitrors.StudyVisitor;
import com.medymatch.handover.utils.excel.ICellUtils;
import com.medymatch.handover.utils.map.IMapUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.dcm4che2.data.DicomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import pl.psnc.scape.dicom.data.DirAnonymizationResults;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

@Service("ExcelRowAnalyzerService")
@Scope("prototype")
@Log
public class ExcelRowAnalyzer {

  @Getter
  private List<String> analyzedDirs;

  @Setter
  private AccessionNumber2Directory accessionNumber2Directory;

  @Getter
  private Map<Integer, String> tagsReplacement;

  @Getter
  private ExcelRowAnalyzerParams excelRowAnalyzerParams;

  @Autowired
  private IMapUtils mapUtils;

  @Autowired
  private ICellUtils cellUtils;

  @Autowired
  private RowCellsValidator rowCellsValidator;

  private DirAnonymizationResults dirAnonymizationResults;

  private Cell falseAccessionCell;

  private Cell handedCell;

  @Autowired
  private IRowGeneralCellsUpdater rowGeneralCellsUpdater;

  public ExcelRowAnalyzer() {
    analyzedDirs = new ArrayList<>();
  }

  public ExcelRowAnalyzer(ExcelRowAnalyzerParams excelRowAnalyzerParams) {
    this.excelRowAnalyzerParams = excelRowAnalyzerParams;
    analyzedDirs = new ArrayList<>();
  }

  public RowAnalysisResult analyzeRow(Row row) {
    int rowNumber = row.getRowNum();
    handedCell = getHandedCell(row);

    Cell originalAccessionCell = getOriginalAccessionCell(row);
    falseAccessionCell = getFalseAccessionCell(row);
    RowAnalysisResult preAnalysisValidationResult = rowCellsValidator.preAnalysisCellsValidation(row, rowNumber, originalAccessionCell, falseAccessionCell, handedCell);
    if (preAnalysisValidationResult != null) {
      rowGeneralCellsUpdater.updateHandedCell(preAnalysisValidationResult, handedCell);
      return preAnalysisValidationResult; // Invalid  or all-ready analyzed row - no need to analyze it.
    }

    dirAnonymizationResults = performRowAnalysis(originalAccessionCell, rowNumber);

    RowAnalysisResult postAnalysisValidationResult = rowCellsValidator.postAnalysisValidation(rowNumber, dirAnonymizationResults);
    if (postAnalysisValidationResult != null) {
      rowGeneralCellsUpdater.updateHandedCell(postAnalysisValidationResult, handedCell);
      return postAnalysisValidationResult;
    }

    updateCells(row, tagsReplacement);
    return dirAnonymizationResults == null ?
        RowAnalysisResult.skipped :
        RowAnalysisResult.analyzed;
  }

  private DicomObject getDicoObject() {
    Entry<String, DicomObject> firstEntry = mapUtils.getFirstEntry(dirAnonymizationResults.getFilesBeforeAnonymization());
    return firstEntry.getValue();
  }

  private void updateCells(Row row, Map<Integer, String> tagReplacements) {
    excelRowAnalyzerParams.getRowCustomCellsUpdater().updateCustomCells(row, getDicoObject());
    rowGeneralCellsUpdater.updateFalseAccessionCell(falseAccessionCell, tagReplacements);
    rowGeneralCellsUpdater.updateHandedCell(RowAnalysisResult.analyzed, handedCell);
  }

  private Cell getFalseAccessionCell(Row row) {
    return cellUtils.getCell(row, excelRowAnalyzerParams.getFalseAccessionColumn(), true);
  }

  private Cell getHandedCell(Row row) {
    return cellUtils.getCell(row, excelRowAnalyzerParams.getHandedColumn(), true);
  }

  private Cell getOriginalAccessionCell(Row row) {
    return cellUtils.getCell(row, excelRowAnalyzerParams.getOriginalAccessionColumn(), false);
  }

  private DirAnonymizationResults performRowAnalysis(Cell originalAccessionCell, int rowNumber) {
    String accessionNumber = cellUtils.getCellValue(originalAccessionCell);
    System.out.println("Analyzing row #" + rowNumber + " with accession number:" + accessionNumber);
    StudyParams studyParams = getStudyParams(accessionNumber);

    Path studyDirectory = accessionNumber2Directory.getDirectory(accessionNumber);
    if (studyDirectory != null) {
      StudyVisitor studyVisitor = new StudyVisitor(accessionNumber, excelRowAnalyzerParams.getTagReplacementAlgorithm());
      DirAnonymizationResults ret = visitStudy(studyVisitor, studyDirectory.toFile(), excelRowAnalyzerParams.getOutputDir());
      tagsReplacement = studyVisitor.getTagsReplacement();
      return ret;
    }
    return null;
  }

  private DirAnonymizationResults visitStudy(StudyVisitor studyVisitor, File studyDirectory, String outputDirectory) {
    try {
      DirAnonymizationResults dirAnonymizationResults;

      studyVisitor.visitStudy(studyDirectory.toPath(), outputDirectory);
      List<String> studyAnalyzedDirs = studyVisitor.getAnalyzedDirs();

      if (studyAnalyzedDirs != null) {
        analyzedDirs.addAll(studyAnalyzedDirs);
      }

      return studyVisitor.getDirAnonymizationResults();
    } catch (IOException e) {
      log.log(Level.WARNING, "Failed to read " + studyDirectory.getAbsolutePath(), e);
      return null;
    }
  }

  private StudyParams getStudyParams(String accessionNumber) {
    StudyParams studyParams = new StudyParams();
    studyParams.setAccessionNumber(accessionNumber);
    studyParams.setSourceId(excelRowAnalyzerParams.getSourceId());
    return studyParams;
  }
}
