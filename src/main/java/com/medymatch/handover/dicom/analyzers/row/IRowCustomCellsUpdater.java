package com.medymatch.handover.dicom.analyzers.row;

import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.dcm4che2.data.DicomObject;

import pl.psnc.scape.dicom.tag.ITagFetcher;

public interface IRowCustomCellsUpdater {
	public void updateCustomCells(Row row, DicomObject dicomObject);
	
	public ITagFetcher getTagFetcher();
	
	public Map<String, String> getCustomColumnMap();
}
