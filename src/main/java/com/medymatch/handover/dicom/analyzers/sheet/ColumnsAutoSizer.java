package com.medymatch.handover.dicom.analyzers.sheet;

import java.util.Map;

import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.medymatch.handover.data.analysis.ExcelRowAnalyzerParams;
import com.medymatch.handover.utils.excel.IColumnUtils;

@Service("ColumnsAutoSizerService")
@Scope("prototype")
public class ColumnsAutoSizer implements IColumnsAutoSizer {
	
	private Sheet sheet;
	private ExcelRowAnalyzerParams excelRowAnalyzerParams;
	
	@Autowired
	private IColumnUtils columnUtils;
	
	public ColumnsAutoSizer(Sheet sheet, ExcelRowAnalyzerParams excelRowAnalyzerParams){
		this.sheet = sheet;
		this.excelRowAnalyzerParams = excelRowAnalyzerParams;
	}
	
	public void autoSizeColumns(){
		autoSizeFalseAccessionColumn();
		autoSizeHandedColumn();
		autoSizeCusomColumns();
	}
	
	private void autoSizeFalseAccessionColumn(){
		String falseAccessionColumnLetter = excelRowAnalyzerParams.getFalseAccessionColumn();
		columnUtils.autoSizeColumn(sheet, falseAccessionColumnLetter);
	}
	
	private void autoSizeHandedColumn(){
		String handedColumnLetter = excelRowAnalyzerParams.getHandedColumn();
		columnUtils.autoSizeColumn(sheet, handedColumnLetter);
	}
	
	private void autoSizeCusomColumns(){
		Map<String, String> customColumnMap = excelRowAnalyzerParams.getCustomColumnMap();
		for (String columnLetter : customColumnMap.values()) {
			columnUtils.autoSizeColumn(sheet, columnLetter);
		}
	}
}
