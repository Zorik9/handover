package com.medymatch.handover.dicom.analyzers.study;

import org.springframework.context.annotation.Scope;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Service;

import com.medymatch.handover.data.analysis.DicomParams;

import pl.psnc.scape.dicom.anonymization.IAnonymizerInitializer;
import pl.psnc.scape.dicom.anonymization.IDirectoryAnonymizer;
import pl.psnc.scape.dicom.data.AnonymizerData;

@Service("DirectoryAnonymizerInitializerService")
@Scope("prototype")
public class DirectoryAnonymizerInitializer implements IDirectoryAnonymizerInitializer {
	
	private AbstractApplicationContext padiMasterContext;
	private IAnonymizerInitializer anonymizerInitializer;

	public DirectoryAnonymizerInitializer(AbstractApplicationContext padiMasterContext){
		this.padiMasterContext = padiMasterContext;
	}
	
	public IDirectoryAnonymizer getDirectoryAnonymizer(DicomParams dicomParams) {
		anonymizerInitializer = (IAnonymizerInitializer)padiMasterContext.getBean("AnonymizerInitializerService");	
		AnonymizerData anonymizerData = new AnonymizerData();
		setAnonymizerData(dicomParams, anonymizerData);		
		IDirectoryAnonymizer directoryAnonymizer = (IDirectoryAnonymizer) padiMasterContext.getBean("DirectoryAnonymizerService", anonymizerData);
		return directoryAnonymizer;
	}

	private void setAnonymizerData(DicomParams dicomParams, AnonymizerData anonymizerData) {
		anonymizerData.setAnonymizerInitializer(anonymizerInitializer);
		anonymizerData.setArchiveTags(false);
		anonymizerData.setConfigFilePath(dicomParams.getConfigFilePath());
		anonymizerData.setTagsReplacement(dicomParams.getTagsReplacement());
	}
}
