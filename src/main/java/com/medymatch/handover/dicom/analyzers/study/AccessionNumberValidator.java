package com.medymatch.handover.dicom.analyzers.study;

import java.io.File;

import org.apache.log4j.Logger;
import org.dcm4che2.data.DicomObject;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Service;

import com.medymatch.handover.config.GlobalDefinitions;

import pl.psnc.scape.dicom.io.reader.IDicomReader;
import pl.psnc.scape.dicom.tag.ITagFetcher;

@Service("AccessionNumberValidatorService")
@Scope("prototype")
public class AccessionNumberValidator {
	private static Logger log = Logger.getLogger(AccessionNumberValidator.class);
			
	private AbstractApplicationContext padiMasterContext;

	private IDicomReader dicomReader;
	
	private ITagFetcher tagFetcher;
	
	public AccessionNumberValidator(AbstractApplicationContext padiMasterContext){
		this.padiMasterContext = padiMasterContext;
		initDicomReader();
		initTagFetcher();
	}

	public boolean isMatchesAccessionNumber(String dicomFilePath, String accessionNumber){
		DicomObject dicomObject = dicomReader.read(dicomFilePath);
		GlobalDefinitions globalDefinitions = new GlobalDefinitions();
		String accessionNumberTagKey = getAccessionNumberTagKey(globalDefinitions);
		
		int accessionNumberTagNumber = tagFetcher.getTag(accessionNumberTagKey);		
		String accessionNumberInFirstDicom = dicomObject.getString(accessionNumberTagNumber);
		
		if(accessionNumberInFirstDicom == null){
			log.warn(String.format("The dicom file %s does not contain accession number",dicomFilePath));
			return false;
		}
		
		return accessionNumberInFirstDicom.equals(accessionNumber);
	}
	
	private String getAccessionNumberTagKey(GlobalDefinitions globalDefinitions) {
		String accessionNumberTagKey = globalDefinitions.getTagNameOpeningBrackets() +
									   globalDefinitions.getAccessionNumberTagKey() + 
									   globalDefinitions.getTagNameClosingBrackets();
		return accessionNumberTagKey;
	}
	
	private void initDicomReader(){
		dicomReader = (IDicomReader)padiMasterContext.getBean("DicomReaderService");
	}
	
	private void initTagFetcher(){
		tagFetcher = (ITagFetcher)padiMasterContext.getBean("TagFetcherService");
	}
}
