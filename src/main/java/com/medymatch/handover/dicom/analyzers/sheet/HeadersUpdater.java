package com.medymatch.handover.dicom.analyzers.sheet;

import java.util.Date;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.medymatch.handover.config.GlobalDefinitions;
import com.medymatch.handover.data.analysis.ExcelRowAnalyzerParams;
import com.medymatch.handover.utils.excel.ICellUtils;

@Service("HeadersUpdaterService")
@Scope("prototype")
public class HeadersUpdater implements IHeadersUpdater {	
	@Autowired
	private ICellUtils cellUtils;
	
	private Sheet sheet;
	
	private int headerRowIndex;
	
	private ExcelRowAnalyzerParams excelRowAnalyzerParams;
	
	public HeadersUpdater(Sheet sheet, ExcelRowAnalyzerParams excelRowAnalyzerParams, int headerRowIndex){
		this.sheet = sheet;
		this.excelRowAnalyzerParams = excelRowAnalyzerParams;
		this.headerRowIndex = headerRowIndex;
	}
	
	public void updateHeaders(){
		Row headerRow = sheet.getRow(headerRowIndex);
		String handedColumn = excelRowAnalyzerParams.getHandedColumn();
		String falseAccessionColumn = excelRowAnalyzerParams.getFalseAccessionColumn();
		String timeStampCellAddress = excelRowAnalyzerParams.getTimestampCell();
		
		updateCustomFieldsHeaders(headerRow, excelRowAnalyzerParams.getCustomColumnMap());
		updateGeneralFieldsHeaders(headerRow, handedColumn, falseAccessionColumn, timeStampCellAddress);
	}

	private void updateCustomFieldsHeaders(Row headerRow, Map<String, String> customColumnMap) {
		String currentColumnLeteer;
		String currentTagTitle;
		GlobalDefinitions globalDefinitions= new GlobalDefinitions();
		
		for (String tagName : customColumnMap.keySet()) {
			currentColumnLeteer = customColumnMap.get(tagName);
			currentTagTitle = tagName.substring(0, tagName.indexOf(globalDefinitions.getTagNameOpeningBrackets()));
			cellUtils.updateCellValue(headerRow, currentColumnLeteer, currentTagTitle);
		}
	}
	
	private void updateGeneralFieldsHeaders(Row headerRow, String handedColumn, String falseAccessionColumn,
			String timeStampCellAddress) {
		cellUtils.updateCellValue(headerRow, handedColumn, "handed");
		cellUtils.updateCellValue(headerRow, falseAccessionColumn, "False Accession Number");
		//cellUtils.updateCellValue(headerRow, falseAccessionColumn, "False Accession Number");
		
		Cell timeStampCell = cellUtils.getCell(sheet, timeStampCellAddress);
		cellUtils.updateCellValue(timeStampCell, new Date().toString());
	}
}
