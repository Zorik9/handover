package com.medymatch.handover.dicom.analyzers.sheet;

import com.medymatch.handover.data.analysis.ExcelRowAnalyzerParams;
import com.medymatch.handover.data.analysis.ExcelSheetAnalyzerParams;
import com.medymatch.handover.data.app.HandoverProperties;
import com.medymatch.handover.data.app.TagReplacementPolicy;
import com.medymatch.handover.dicom.ITagReplacementAlgorithm;
import com.medymatch.handover.dicom.TagReplacementAlgorithm;
import com.medymatch.handover.dicom.analyzers.row.ExcelRowAnalyzer;
import com.medymatch.handover.dicom.analyzers.row.IRowCustomCellsUpdater;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Service;
import pl.psnc.scape.dicom.tag.ITagFetcher;

@Service("SheetAnalyzerFactoryService")
@Scope("prototype")
public class SheetAnalyzerFactory implements ISheetAnalyzerFactory {
	
	private AbstractApplicationContext context;
	private HandoverProperties handoverProperties;
	private Sheet sheet;
	private String tagsPropertiesFile;
	private String inputDir;
	
	public SheetAnalyzerFactory(AbstractApplicationContext context, HandoverProperties handoverProperties, String inputDir, Sheet sheet, String tagsPropertiesFile){
		this.context = context;
		this.handoverProperties = handoverProperties;
		this.inputDir = inputDir;
		this.sheet = sheet;
		this.tagsPropertiesFile = tagsPropertiesFile;
	}
	
	public IExcelSheetAnalyzer getSheetAnalyzer(){
		ExcelRowAnalyzerParams excelRowAnalyzerParams = getExcelRowAnalyzerParams();
		
		ExcelRowAnalyzer excelRowAnalyzer = (ExcelRowAnalyzer)context.getBean("ExcelRowAnalyzerService", excelRowAnalyzerParams);
		
		IColumnsAutoSizer columnsAutoSizer = (IColumnsAutoSizer)context.getBean("ColumnsAutoSizerService", sheet, excelRowAnalyzerParams);
		IHeadersUpdater headersUpdater = (IHeadersUpdater)context.getBean("HeadersUpdaterService", sheet, excelRowAnalyzerParams, handoverProperties.getFirstDataRow() - 1);
		
		ExcelSheetAnalyzerParams excelSheetAnalyzerParams = getSheetAnalyzerParams(excelRowAnalyzer, excelRowAnalyzerParams, columnsAutoSizer, headersUpdater);
		
		return getSheetAnalyzer(excelSheetAnalyzerParams);
	}
	
	private ExcelSheetAnalyzerParams getSheetAnalyzerParams(ExcelRowAnalyzer excelRowAnalyzer, ExcelRowAnalyzerParams excelRowAnalyzerParams, IColumnsAutoSizer columnsAutoSizer, IHeadersUpdater headersUpdater){
		ExcelSheetAnalyzerParams excelSheetAnalyzerParams = new ExcelSheetAnalyzerParams();
		excelSheetAnalyzerParams.setColumnsAutoSizer(columnsAutoSizer);
		excelSheetAnalyzerParams.setExcelRowAnalyzer(excelRowAnalyzer);
		excelSheetAnalyzerParams.setHandoverProperties(handoverProperties);
		excelSheetAnalyzerParams.setHeadersUpdater(headersUpdater);
		excelSheetAnalyzerParams.setSheet(sheet);
		return excelSheetAnalyzerParams;
	}
	
	private IExcelSheetAnalyzer getSheetAnalyzer(ExcelSheetAnalyzerParams excelSheetAnalyzerParams){
		IExcelSheetAnalyzer excelSheetAnalyzer = (IExcelSheetAnalyzer)context.getBean("ExcelSheetAnalyzerService", excelSheetAnalyzerParams);
		return excelSheetAnalyzer;
	}
	
	private ExcelRowAnalyzerParams getExcelRowAnalyzerParams() {
		ExcelRowAnalyzerParams excelRowAnalyzerParams = new ExcelRowAnalyzerParams();
		setGeneralParams(excelRowAnalyzerParams);
		setTagReplacementAlgorithm(excelRowAnalyzerParams);
		setRowCustomCellsUpdater(excelRowAnalyzerParams);

		return excelRowAnalyzerParams;
	}

	private void setGeneralParams(ExcelRowAnalyzerParams excelRowAnalyzerParams) {
		excelRowAnalyzerParams.setInputDir(inputDir);
		excelRowAnalyzerParams.setOutputDir(handoverProperties.getOutputDir());
		excelRowAnalyzerParams.setOriginalAccessionColumn(handoverProperties.getOriginalAccessionColumn());
		excelRowAnalyzerParams.setSourceId(handoverProperties.getSourceId());
		excelRowAnalyzerParams.setFalseAccessionColumn(handoverProperties.getFalseAccessionColumn());
		excelRowAnalyzerParams.setHandedColumn(handoverProperties.getHandedColumn());
		excelRowAnalyzerParams.setCustomColumnMap(handoverProperties.getCustomColumnMap());
		excelRowAnalyzerParams.setTimestampCell(handoverProperties.getTimestampCell());
	}

	private void setTagReplacementAlgorithm(ExcelRowAnalyzerParams excelRowAnalyzerParams) {
		TagReplacementPolicy tagReplacementPolicy = new TagReplacementPolicy(handoverProperties.getSourceId(), -1);
		ITagReplacementAlgorithm tagReplacementAlgorithm = (TagReplacementAlgorithm)context.getBean("TagReplacementAlgorithmService", tagReplacementPolicy, tagsPropertiesFile);
		excelRowAnalyzerParams.setTagReplacementAlgorithm(tagReplacementAlgorithm);
	}
	
	private void setRowCustomCellsUpdater(ExcelRowAnalyzerParams excelRowAnalyzerParams) {
		AbstractApplicationContext padiMasterContext = new AnnotationConfigApplicationContext(pl.psnc.scape.dicom.config.AppConfig.class);
		ITagFetcher tagFetcher = (ITagFetcher)padiMasterContext.getBean("TagFetcherService");
		padiMasterContext.close();
		IRowCustomCellsUpdater rowCustomCellsUpdater = (IRowCustomCellsUpdater)context.getBean("RowCustomCellsUpdaterService", tagFetcher, handoverProperties.getCustomColumnMap());
		excelRowAnalyzerParams.setRowCustomCellsUpdater(rowCustomCellsUpdater);
	}
}
