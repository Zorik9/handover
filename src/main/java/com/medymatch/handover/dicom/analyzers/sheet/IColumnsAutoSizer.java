package com.medymatch.handover.dicom.analyzers.sheet;

public interface IColumnsAutoSizer {
	public void autoSizeColumns();
}
