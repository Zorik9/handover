package com.medymatch.handover.dicom.analyzers.study;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dcm4che2.data.DicomObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Service;

import com.medymatch.handover.config.GlobalDefinitions;
import com.medymatch.handover.data.analysis.DicomParams;
import com.medymatch.handover.dicom.visitrors.StudyVisitor;
import com.medymatch.handover.utils.file.IDirectoryCopier;
import com.medymatch.handover.utils.file.IFilePathBuilder;

import lombok.Getter;
import pl.psnc.scape.dicom.anonymization.IDirectoryAnonymizer;
import pl.psnc.scape.dicom.data.DirAnonymizationResults;
import pl.psnc.scape.dicom.io.reader.DicomReader;
import pl.psnc.scape.dicom.io.reader.IDicomReader;

@Service("StudyAnalyzerService")
@Scope("prototype")
public class StudyAnalyzer {
	
	private static Logger log = Logger.getLogger(StudyAnalyzer.class);
	
//	@Autowired
//	private IFilePathBuilder filePathBuilder;
//	
//	@Autowired
//	private IDirectoryCopier directoryCopier;
	
	@Getter
	private List<String> analyzedDirs;
	
	private IDicomReader dicomReader;
	
	private AbstractApplicationContext padiMasterContext;
	
	private IDirectoryAnonymizerInitializer directoryAnonymizerInitializer;

	public StudyAnalyzer(AbstractApplicationContext padiMasterContext){
		this.padiMasterContext = padiMasterContext;
		initDicomReader();
		initDirectoryAnonymizerInitializer();
	}
	
	public DirAnonymizationResults analyzeStudy(DicomParams dicomParams){
		IDirectoryAnonymizer directoryAnonymizer = directoryAnonymizerInitializer.getDirectoryAnonymizer(dicomParams);
		
		File[] listFiles = new File(dicomParams.getInputDirPath()).listFiles();
		Map<String, DicomObject> filesBeforeAnonymizationMap = getFilesBeforeAnonymization(listFiles);
		
		DirAnonymizationResults dirAnonymizationResults = directoryAnonymizer.anonimizeDir(dicomParams.getInputDirPath());
		dirAnonymizationResults.setFilesBeforeAnonymization(filesBeforeAnonymizationMap);
		
		if(analyzedDirs == null){
			analyzedDirs = new ArrayList<>();
		}
		analyzedDirs.add(dicomParams.getInputDirPath());
		
		return dirAnonymizationResults;
	}
	
//	private void copyFileToTempDir(Path file){
//		
//		GlobalDefinitions globalDefinitions = new GlobalDefinitions();
//		
//		String infoMessage = String.format("Copy dicom files to the temporary directory: %s ...", globalDefinitions.getTempFolder());
//		System.out.println(infoMessage);
//		log.info(infoMessage);
//		directoryCopier.copyDirToDir(file.toString(), globalDefinitions.getTempFolder());
//	}
	
	private Map<String, DicomObject> getFilesBeforeAnonymization(File[] listFiles) {
		Map<String, DicomObject> filesBeforeAnonymization = new HashMap<>();
		String filePath;
		
		for (File file : listFiles) {
			filePath = file.getAbsolutePath();
			filesBeforeAnonymization.put(filePath, dicomReader.read(filePath));
		}
		return filesBeforeAnonymization;
	}
	
	private void initDirectoryAnonymizerInitializer(){
		directoryAnonymizerInitializer = new DirectoryAnonymizerInitializer(padiMasterContext);
	}
	
	private void initDicomReader(){
		dicomReader = new DicomReader();
	}
}
