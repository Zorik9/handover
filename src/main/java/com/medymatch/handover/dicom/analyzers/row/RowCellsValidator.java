package com.medymatch.handover.dicom.analyzers.row;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.medymatch.handover.config.DicomConfig;
import com.medymatch.handover.config.GlobalDefinitions;
import com.medymatch.handover.data.analysis.RowAnalysisResult;
import com.medymatch.handover.utils.excel.ICellUtils;

import pl.psnc.scape.dicom.data.DirAnonymizationResults;

@Service("RowCellsValidatorService")
public class RowCellsValidator {
	
	private DicomConfig dicomConfig = new DicomConfig();
	private GlobalDefinitions globalDefinitions = new GlobalDefinitions();
	
	@Autowired
	private ICellUtils cellUtils;

	public RowAnalysisResult postAnalysisValidation(int rowNumber, DirAnonymizationResults dirAnonymizationResults) {
		if(dirAnonymizationResults == null){
			System.out.println("Could not process row #" + rowNumber + ", because the accession number did not match to any DICOM file in the input folder.");
        	return RowAnalysisResult.skipped;
		}
		return null;
	}
	
	public RowAnalysisResult preAnalysisCellsValidation(Row row, int rowNumber, Cell originalAccessionCell, Cell falseAccessionCell, Cell statusCell) {
		RowAnalysisResult AccessionCellValidationResult = validateOriginalAccessionCell(rowNumber, originalAccessionCell);
		if(AccessionCellValidationResult != null){
			return AccessionCellValidationResult;
		}
	    
	    RowAnalysisResult falseAccessionCellValidationResult = validateFalseAccessionCell(rowNumber, falseAccessionCell);
		if(falseAccessionCellValidationResult != null){
			System.out.println("Skipping row #" + rowNumber + ", because false accession cell already has a value.");
			return falseAccessionCellValidationResult;
		}

		if (rowPreviouslySkipped(statusCell)) {
			System.out.println("Skipping row #" + rowNumber + " which was already skipped in the past.");
			return RowAnalysisResult.skipped;
		}
		
		return null;
	}

	private boolean rowPreviouslySkipped(Cell statusCell) {
		return RowAnalysisResult.skipped.toString().equalsIgnoreCase(cellUtils.getCellValue(statusCell));
	}

	private RowAnalysisResult validateFalseAccessionCell(int rowNumber, Cell falseAccessionCell) {
		String falseAccessionCurrentValue = cellUtils.getCellValue(falseAccessionCell);
	    if(!StringUtils.isEmpty(falseAccessionCurrentValue)){
	    	if(falseAccessionCurrentValue.matches(globalDefinitions.getAccessionNumberPattern())){
	    		if(dicomConfig.isReanalyze()){
	    			return null; // This row should be analyzed.
	    		}	else{
	    			return RowAnalysisResult.alreadyAnalyzed;
	    		}
	    	}
	    	else{
				System.out.println("Row #" + rowNumber + " contains a non-numeric accession number.");
				return RowAnalysisResult.invalid;
	    	}
	    }
	    
	    return null;
	}

	private RowAnalysisResult validateOriginalAccessionCell(int rowNumber, Cell originalAccessionCell) {
		if(cellUtils.isEmptyCell(originalAccessionCell)){
	    	System.out.println("Skipping row #" + rowNumber + ", because original accession cell is empty.");
	    	return RowAnalysisResult.invalid;
	    }
		return null;
	}
}
