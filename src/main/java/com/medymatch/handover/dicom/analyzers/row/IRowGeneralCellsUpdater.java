package com.medymatch.handover.dicom.analyzers.row;

import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;

import com.medymatch.handover.data.analysis.RowAnalysisResult;

public interface IRowGeneralCellsUpdater {
	public void updateHandedCell(RowAnalysisResult rowAnalysisResult, Cell handedCell);
	
	public void updateFalseAccessionCell(Cell falseAccessionCell, Map<Integer, String> tagsReplacement);
}
