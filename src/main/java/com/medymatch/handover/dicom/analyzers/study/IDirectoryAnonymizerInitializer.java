package com.medymatch.handover.dicom.analyzers.study;

import com.medymatch.handover.data.analysis.DicomParams;

import pl.psnc.scape.dicom.anonymization.IDirectoryAnonymizer;

public interface IDirectoryAnonymizerInitializer {
	public IDirectoryAnonymizer getDirectoryAnonymizer(DicomParams dicomParams);
}
