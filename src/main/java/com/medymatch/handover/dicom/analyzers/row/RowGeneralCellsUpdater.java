package com.medymatch.handover.dicom.analyzers.row;

import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.medymatch.handover.config.GlobalDefinitions;
import com.medymatch.handover.data.analysis.RowAnalysisResult;
import com.medymatch.handover.utils.excel.ICellUtils;

import pl.psnc.scape.dicom.tag.TagFetcher;

@Service("RowGeneralCellsUpdaterService")
public class RowGeneralCellsUpdater implements IRowGeneralCellsUpdater {
	@Autowired
	private ICellUtils cellUtils;
	
	public void updateHandedCell(RowAnalysisResult rowAnalysisResult, Cell handedCell) {
		cellUtils.updateCellValue(handedCell, rowAnalysisResult.name());
	}

	public void updateFalseAccessionCell(Cell falseAccessionCell, Map<Integer, String> tagsReplacement) {
		String falseAccessionNumber = getFalseAccessionNumber(tagsReplacement);
		cellUtils.updateCellValue(falseAccessionCell, falseAccessionNumber);
	}
	
	private String getFalseAccessionNumber(Map<Integer, String> tagsReplacement){
		GlobalDefinitions globalDefinitions = new GlobalDefinitions(); 
		String accessionTagKey = globalDefinitions.getAccessionNumberTagName();
		int accessionTagId = new TagFetcher().getTag(accessionTagKey);
		String falseAccessionNumber = tagsReplacement.get(accessionTagId);
		return falseAccessionNumber;
	}
}
