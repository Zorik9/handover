package com.medymatch.handover.dicom.output;

import java.io.File;
import java.util.List;

public interface IAnonymizedDirsCopier {
	public void copyAnonymizedFolders(File srcDir, List<String> analyzedDirs, File destDir);
}
