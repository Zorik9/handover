package com.medymatch.handover.dicom.output;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.medymatch.handover.utils.file.IFilePathBuilder;

@Service("ResultsCopierService")
@Scope("prototype")
public class ResultsCopier {
	private static Logger log = Logger.getLogger(ResultsCopier.class);
	
	private String xlsFile;
	private String dicomFolderName;
	private String outputDir;
	private File tempDir;
	
	@Autowired
	IFilePathBuilder filePathBuilder;
	
	
	@Autowired
	IAnonymizedDirsCopier anonymizedDirsCopier;
	
	@Autowired
	private IXlsOverrider xlsOverrider;
	
	public ResultsCopier(){}
	
	public ResultsCopier(String xlsFile, String dicomFolderName, String outputDir, File tempDir) {
		this.xlsFile = xlsFile;
		this.dicomFolderName = dicomFolderName;
		this.outputDir = outputDir;
		this.tempDir = tempDir;
	}

	
	public void copyDicomResultsToOutputDir(List<String> analyzedDirs){
		log.info("Overring excel input file and excel backup file...\n");
		System.out.println("Updating analysis results in the input excel file...\n");
		xlsOverrider.overrideInputXls(xlsFile, tempDir);
	}
	

}

