package com.medymatch.handover.dicom.output;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.io.Files;
import com.medymatch.handover.config.GlobalDefinitions;
import com.medymatch.handover.utils.file.IFileMover;
import com.medymatch.handover.utils.file.IFilePathBuilder;

@Service("XlsOverriderService")
public class XlsOverrider implements IXlsOverrider {
	
	@Autowired
	private IFilePathBuilder filePathBuilder;
	
	@Autowired
	private IFileMover fileMover;
	
	private GlobalDefinitions globalDefinitions = new GlobalDefinitions();
	
	public void overrideInputXls(String xlsFilePath, File tempDir){
		File xlsFile = new File(xlsFilePath);
		overrideXlsBckFile(xlsFilePath, xlsFile);
		overrideXlsFile(tempDir, xlsFile);
	}

	private void overrideXlsBckFile(String xlsFilePath, File xlsFile) {
		File backupXlsFile = backupXlsFile(xlsFilePath, xlsFile);
		if(backupXlsFile.exists()){
			backupXlsFile.delete();
		}
		fileMover.moveFile(xlsFile, backupXlsFile, "excel backup file");
	}
	
	private File backupXlsFile(String xlsFilePath, File xlsFile){
		File xlsDir = filePathBuilder.getParentFile(xlsFile);
		String backupXlsFileName = Files.getNameWithoutExtension(xlsFilePath) + globalDefinitions.getXlsBackupPostfix();
		String backupXlsFilePath = filePathBuilder.getFilePath(xlsDir, backupXlsFileName);
		return new File(backupXlsFilePath);
	}

	private void overrideXlsFile(File tempDir, File xlsFile) {
		String tempXlsFilePath = filePathBuilder.getFilePath(tempDir, xlsFile.getName());
		File tempXlsFile = new File(tempXlsFilePath);
		fileMover.moveFile(tempXlsFile, xlsFile, "original excel file");
	}
}
