package com.medymatch.handover.dicom.output;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.medymatch.handover.config.GlobalDefinitions;
import com.medymatch.handover.utils.file.IDirectoryCopier;

@Service("AnonymizedDirsCopierService")
public class AnonymizedDirsCopier implements IAnonymizedDirsCopier {
	private static Logger log = Logger.getLogger(AnonymizedDirsCopier.class);
	
	private GlobalDefinitions globalDefinitions = new GlobalDefinitions();
	
	@Autowired
	IDirectoryCopier directoryCopier;
	
	public void copyAnonymizedFolders(File srcDir, List<String> analyzedDirs, File destDir){
		if(destDir == null){
			throw new RuntimeException("Could not copy anonymized folders, due to error with creating the target folder.");
		}
		
		destDir.mkdirs();
		File[] files = getFileList(srcDir);
		
		copyAnonymizedFolders(destDir, analyzedDirs, files);
	}

	private File[] getFileList(File srcDir) {
		File[] files = srcDir.listFiles();
		if(files == null){
			files = new File[]{srcDir}; // Single file
		}
		return files;
	}
	
	private void copyAnonymizedFolders(File destDir, List<String> analyzedDirs, File[] files) {
        for (int i = 0; i < files.length; i++) {
        	if(files[i].isDirectory()){
        		if(isAnalyzedDir(analyzedDirs, files[i])){
        			copyAnonymizedFolders(destDir, analyzedDirs, files[i].listFiles());
        		}
        		else{
        			log.debug("Skipping the folder %s, because it was not analyzed.");
        		}
        	}
        	else{
	            copyDicomFile(destDir, files[i]);
        	}
    	}
	}

	private boolean isAnalyzedDir(List<String> analyzedDirs, File file) {
		return analyzedDirs.contains(file.getAbsolutePath());
	}

	private void copyDicomFile(File destDir, File file) {
		if(isDicomFile(file)){
			directoryCopier.copyFileToTargetDir(destDir, file, "dicom file");
		}
		else {
			log.debug(String.format("Skipping the copying the file %s, because it's not a dicom file", file));
		}
	}

	private boolean isDicomFile(File file){
		return file.getName().toLowerCase().endsWith(globalDefinitions.getDicomFileExtenssion()) ||
           	   file.getName().toLowerCase().endsWith(globalDefinitions.getDicomFileExtenssion() + "." + globalDefinitions.getTextFileExtenssion());
	}
}
