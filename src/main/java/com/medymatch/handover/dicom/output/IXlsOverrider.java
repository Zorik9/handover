package com.medymatch.handover.dicom.output;

import java.io.File;

public interface IXlsOverrider {
	public void overrideInputXls(String xlsFile, File tempDir);
}
