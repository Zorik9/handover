package com.medymatch.handover.dicom;

import com.medymatch.handover.data.analysis.DicomParams;

public interface ITagReplacementAlgorithm {
	public DicomParams applyPolicy();
}
