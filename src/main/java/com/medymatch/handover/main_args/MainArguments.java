package com.medymatch.handover.main_args;

import org.springframework.stereotype.Repository;

import com.beust.jcommander.Parameter;

import lombok.Data;

@Data
@Repository
public class MainArguments {
    @Parameter(names = "-xlsFile", converter = FilePathConverter.class, description = "The path of the excel file that contains the input accession numbers", required = false)
    private String xlsFile;
    
    @Parameter(names = "-dicomRootFolder", converter = FilePathConverter.class, description = "The path of folder that contains the dicom studies. Each study contains a set of dicom files.", required = false)
    private String DicomRootFolder;
	
    @Parameter(names = "-appPropertiesFile", converter = FilePathConverter.class, description = "The path of the application properties file", required = false)
    private String appPropertiesFile;
    
    @Parameter(names = "-tagsPropertiesFile", converter = FilePathConverter.class, description = "The path of the dicom tags properties file", required = false)
    private String tagsPropertiesFile;
}
