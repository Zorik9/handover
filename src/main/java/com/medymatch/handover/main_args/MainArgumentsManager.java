package com.medymatch.handover.main_args;

import org.apache.log4j.Logger;

import com.beust.jcommander.JCommander;
import com.medymatch.handover.config.LoggerLoader;
import com.medymatch.handover.dicom.transaction.TransactionManager;

public class MainArgumentsManager {
	private static Logger log = Logger.getLogger(MainArgumentsManager.class);
	
	public MainArguments getMainArguments(String[] args){
		LoggerLoader LoggerLoader = new LoggerLoader();
		LoggerLoader.loadLogger("/log4j.properties", "log4j");
		
		if(args == null || args.length == 0){
			log.info("No arguments are given from main");
		}
		
		MainArguments mainArguments = new MainArguments();
		
		log.info("Parsing Main arguments.");
		new JCommander(mainArguments, args);
		return mainArguments;
	}
	
	public void handleMainArguments(MainArguments mainArguments){
		
		if(!doTransaction(mainArguments)){
			System.out.println("Some errors have occured during the anonymization process.\nPlease check the logs.");
			System.exit(1);
		}
		else{
			System.out.println("Anonymization process finished successfully.");
		}
	}
	
	public boolean doTransaction(MainArguments mainArguments){
		TransactionManager transactionManager = new TransactionManager(mainArguments);
		return transactionManager.doTransaction();
	}
}
