package com.medymatch.handover.main_args;

import java.nio.file.InvalidPathException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.ParameterException;
import com.medymatch.handover.utils.file.IFilePathBuilder;

public class FilePathConverter implements IStringConverter<String>{
	private static Logger log = Logger.getLogger(FilePathConverter.class);
	
	@Autowired
	IFilePathBuilder filePathBuilder;
	
	@Override
	public String convert(String filePath) {
    	try{
    		return filePathBuilder.getPath(filePath);
    	}
    	catch(InvalidPathException ipe){
    		String errorMessage = String.format("The file path %s is invalid.", filePath);
    		log.error(errorMessage, ipe);
    		throw new ParameterException(errorMessage);
    	}
	}
}
