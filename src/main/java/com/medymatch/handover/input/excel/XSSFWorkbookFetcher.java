package com.medymatch.handover.input.excel;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XSSFWorkbookFetcher {
	public XSSFWorkbook fetchXSSFWorkbook(File workbookFile){
		try(OPCPackage pkg = OPCPackage.open(workbookFile)){
			return new XSSFWorkbook(pkg);
		} catch (InvalidFormatException | IOException e) {
			throw new RuntimeException("Failed to fetch XSSF workbook", e);
		}
	}
}
