package com.medymatch.handover.input.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class HSSFWorkbookFetcher {
	public HSSFWorkbook fetchHSSFWorkbook(File filworkbookFilee){
		try(POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(filworkbookFilee))){
			return new HSSFWorkbook(fs.getRoot(), true);
		}
		catch (IOException e) {
			throw new RuntimeException("Failed to fetch HSSF workbook", e);
		}
	}
}
