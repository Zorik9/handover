package com.medymatch.handover.input.excel;

import org.springframework.util.StringUtils;

import com.google.common.io.Files;
import com.medymatch.handover.data.app.ExcelFileExtension;

public class ExcelFileExtensionFetcher {
	public ExcelFileExtension fetchExcelFileExtension(String filePath){
		if(StringUtils.isEmpty(filePath)){
			return null;
		}
		
		String fileExtension = Files.getFileExtension(filePath);
		return ExcelFileExtension.valueOf(fileExtension);
	}
}
