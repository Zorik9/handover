package com.medymatch.handover.input.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("ExcelWorkbookReaderService")
@Scope("prototype")
public class ExcelWorkbookReader {
	
	private File excelFile;
	
	public ExcelWorkbookReader(File excelFile){
		this.excelFile = excelFile;
	}
	
	public ExcelWorkbookReader(String excelFilePath){
		excelFile = new File(excelFilePath);
	}
	
	public Workbook readWorkbook(){
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(excelFile);
		} catch (FileNotFoundException fnfe) {
			throw new RuntimeException("Could not read excel work book, because the input excel file could not be found", fnfe);
		}
		
		Workbook workbook = null;
		try {
			workbook = WorkbookFactory.create(inputStream);
		} catch (EncryptedDocumentException ede) {
			throw new RuntimeException("Workbook factory failed to create workbook, due to document encryption issues.", ede);
		} catch (InvalidFormatException ife) {
			throw new RuntimeException("Workbook factory failed to create workbook, because the workbook format is invalid.", ife);
		} catch (IOException ie) {
			throw new RuntimeException("Workbook factory failed to create workbook, due to IO issues..", ie);
		}
		return workbook;
	}
}
