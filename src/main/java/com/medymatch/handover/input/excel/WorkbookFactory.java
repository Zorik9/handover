package com.medymatch.handover.input.excel;

import java.io.File;

import org.apache.poi.ss.usermodel.Workbook;

import com.medymatch.handover.data.app.ExcelFileExtension;

public class WorkbookFactory {

	public Workbook getWorkbook(File workbookFile){
		Workbook wb = null;
		ExcelFileExtension excelFileExtension = new ExcelFileExtensionFetcher().fetchExcelFileExtension(workbookFile.getAbsolutePath());
		
		switch(ExcelFileExtension.values()[excelFileExtension.ordinal()]){
		case xls:
			return new HSSFWorkbookFetcher().fetchHSSFWorkbook(workbookFile); 
		case xlsx:
			return new XSSFWorkbookFetcher().fetchXSSFWorkbook(workbookFile);
		}
			
		return wb;
	}
}
