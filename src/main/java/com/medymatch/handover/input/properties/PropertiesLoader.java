package com.medymatch.handover.input.properties;

import java.io.File;
import java.nio.file.Path;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.medymatch.handover.utils.file.IProjectRootFetcher;

@Service("PropertiesLoaderService")
@Configurable
public class PropertiesLoader implements IPropertiesLoader{
	private static Logger log = Logger.getLogger(PropertiesLoader.class);
	
	@Autowired
	private IProjectRootFetcher projectRootFetcher;
	
	@Autowired
	private IPropertiesReader propertiesReader;
	
	public Properties readPropertiesFromResourceFolder(String propertiesFileName){
		
		if(StringUtils.isEmpty(propertiesFileName)){
			log.fatal("Failed to read properties file, because the given properties file is empty.");
			return null;
		}

		log.info(String.format("Reading the properties file '%s'...", propertiesFileName));
		Path projRootPath = projectRootFetcher.getProjectRootPath();
		String resourcesPath = projRootPath + File.separator + "src" + File.separator + "main" + File.separator + "resources";
		String propertiesFilePath = resourcesPath + File.separator + propertiesFileName;
		return readPropertiesFromFile(propertiesFilePath);
	}
	
	public Properties readPropertiesFromFile(String propertiesFilePath){
		if(StringUtils.isEmpty(propertiesFilePath)){
			log.info("Cannot read properties, because properties file path is empty.");
			return null;
		}
		
		return propertiesReader.readProperties(propertiesFilePath);
	}
}
