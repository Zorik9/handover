package com.medymatch.handover.input.properties;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.medymatch.handover.data.app.HandoverProperties;
import com.medymatch.handover.functional_interfaces.IConverter;
import com.medymatch.handover.utils.file.IFilePathBuilder;


@Service("HandoverPropertiesLoaderService")
@Scope("prototype")
public class HandoverPropertiesLoader implements IHandoverPropertiesLoader{
	
	@Autowired
	IConverter<Properties, HandoverProperties> handOverPropertiesConverter;
	
	@Autowired
	IFilePathBuilder filePathBuilder;
	
	public HandoverProperties getHandoverProperties(Properties appProperties) {
		
		setPathProperties(appProperties);
		return handOverPropertiesConverter.convertTo(appProperties);
	}
	
	private void setPathProperties(Properties appProperties){
		setPathProperty(appProperties, "dicom-root-folder");
		setPathProperty(appProperties, "xls-file");
		setPathProperty(appProperties, "output-dir");
	}
	
	private void setPathProperty(Properties appProperties, String key){
		String pathProperty = appProperties.getProperty(key);
		if(pathProperty != null){
			appProperties.setProperty(key, filePathBuilder.getPath(pathProperty));
		}
	}
}
