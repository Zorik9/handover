package com.medymatch.handover.input.properties;

import java.util.Properties;

import com.medymatch.handover.data.app.HandoverProperties;

public interface IHandoverPropertiesLoader {
	public HandoverProperties getHandoverProperties(Properties appProperties);
}