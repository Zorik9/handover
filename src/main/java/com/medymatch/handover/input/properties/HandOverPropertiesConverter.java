package com.medymatch.handover.input.properties;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.medymatch.handover.data.app.HandoverProperties;
import com.medymatch.handover.functional_interfaces.IConverter;
import com.medymatch.handover.utils.reflection.IMethodInvoker;
import com.medymatch.handover.utils.string.IStringCapitilizer;

@Service("HandOverPropertiesConverterService")
@Configurable
public class HandOverPropertiesConverter implements IConverter<Properties, HandoverProperties> {
	
	@Autowired
	private IStringCapitilizer stringCapitilizer;
	
	@Autowired
	private IMethodInvoker methodInvoker;
	
	@Override
	public HandoverProperties convertTo(Properties appProperties) {
		String fieldKey;
		String fieldValue;
		HandoverProperties handoverProperties = new HandoverProperties();
		Map<String, Class<?>> fieldTypeMap = getFieldTypeMap();
		Map<String, String> customColumnMap = new HashMap<>();
		
		for (Iterator<Object> it = appProperties.keySet().iterator(); it.hasNext();) {
			fieldKey = (String) it.next();
			fieldValue = appProperties.getProperty(fieldKey);
			
			addField(fieldKey, fieldValue, handoverProperties, fieldTypeMap, customColumnMap);
		}
		
		if(customColumnMap.size() > 0){
			handoverProperties.setCustomColumnMap(customColumnMap);
		}

		return handoverProperties;
	}

	private void addField(String fieldKey, String fieldValue, HandoverProperties handoverProperties,
			Map<String, Class<?>> fieldTypeMap, Map<String, String> customColumnMap) {
		if(fieldKey.startsWith("custom-column")){
			addCustomField(fieldValue, customColumnMap);
		}
		else{
			addGeneralField(fieldTypeMap, fieldKey, fieldValue, handoverProperties);
		}
	}
	
	private void addGeneralField(Map<String, Class<?>> fieldTypeMap, String fieldKey, String fieldValue, HandoverProperties handoverProperties){
		StringBuilder destinationField = stringCapitilizer.capitilizeProperty(fieldKey, '-');
		Class<?> fieldType = fieldTypeMap.get(stringCapitilizer.deCapitilize(destinationField.toString()));
		methodInvoker.reflectionCall(handoverProperties, "HandoverProperties", "set" + destinationField, fieldType, fieldValue);
	}
	
	private void addCustomField(String customField, Map<String, String> customColumnMap){
		String separator = ">>";
		int indexOfSeparator = customField.indexOf(separator);
		int separatorLength = separator.length();
		
		if(indexOfSeparator == -1){
			return;
		}
		
		String tagName = customField.substring(0, indexOfSeparator);
		String columnLetter = customField.substring(indexOfSeparator + separatorLength);
		customColumnMap.put(tagName, columnLetter);
	}

	private Map<String, Class<?>> getFieldTypeMap() {
		Field[] fields = HandoverProperties.class.getDeclaredFields();
		Map<String, Class<?>> fieldTypeMap = new HashMap<>();
		for (Field field : fields) {
			fieldTypeMap.put(field.getName(), field.getType());
		}
		return fieldTypeMap;
	}
}