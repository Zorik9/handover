package com.medymatch.handover.input.properties;

import java.util.Properties;

public interface IPropertiesLoader {
	public Properties readPropertiesFromResourceFolder(String propertiesFileName);
	
	public Properties readPropertiesFromFile(String propertiesFilePath);
}
