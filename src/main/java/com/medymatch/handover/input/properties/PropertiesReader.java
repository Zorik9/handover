package com.medymatch.handover.input.properties;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.Scanner;

import org.springframework.stereotype.Service;

@Service("PropertiesReaderService")
public class PropertiesReader implements IPropertiesReader{
	
	public Properties readProperties(String filePath){
		if(filePath == null){
			return null;
		}
		return readProperties(new File(filePath));
	}
	
	public Properties readProperties(File file){
		Scanner scan = getScanner(file);   
		scan.useDelimiter("\\Z");   
		String content = scan.next();
		return getProperties(content);
	}
	
	private Properties getProperties(String content){
	    Properties properties = new Properties();
	    
	    String[] lines = content.split("\n");
	    int indexOfSeperator = -1;
	    String key;
	    String value;
	    
	    for (String lineContent : lines) {
				String line = lineContent.trim();
	    	if(line.startsWith("#") || line.isEmpty()){
	    		continue;
	    	}
	    	indexOfSeperator = line.indexOf('=');
	    	key = line.substring(0, indexOfSeperator).replace("\r", "");
	    	value = line.substring(indexOfSeperator + 1).replace("\r", "");
	    	properties.setProperty(key, value);
		}
		
		return properties;
	}

	private Scanner getScanner(File file) {
		Scanner scan = null;
		try {
			scan = new Scanner(file);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(String.format("Could not read properties file, because the file %s could not be found", file.getAbsolutePath(), e));
		}
		return scan;
	}
}
