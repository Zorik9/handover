package com.medymatch.handover.input.properties;

import java.io.File;
import java.util.Properties;

public interface IPropertiesReader {
	public Properties readProperties(String filePath);
	
	public Properties readProperties(File file);
}
