package com.medymatch.handover.data.app;

import java.io.File;

import org.springframework.stereotype.Repository;

import lombok.Data;

@Data
@Repository
public class TransactionInput {
	private File tempDir;
	private String xlsFile;
	private String workingDirXlsFile;
	private String workingDirDicomRootPath;
	private String dicomRootFolder;
	private String tagsPropertiesFile;
	private HandoverProperties handoverProperties;
	private String tempWorkingDir;
}
