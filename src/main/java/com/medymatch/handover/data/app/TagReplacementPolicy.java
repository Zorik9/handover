package com.medymatch.handover.data.app;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class TagReplacementPolicy {
	
	private int numOfWantedDigits;
	private List<String> tagReplacementKeys;
	private String prefix;
	private long nowTime;

	public TagReplacementPolicy(String sourceId, long nowTime){
		numOfWantedDigits = 7;
		tagReplacementKeys = new ArrayList<>();
		tagReplacementKeys.add("AccessionNumber(0008,0050)");
		prefix = sourceId;
		this.nowTime = nowTime;
	}
}
