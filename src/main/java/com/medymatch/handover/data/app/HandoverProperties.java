package com.medymatch.handover.data.app;

import java.util.Map;

import org.springframework.stereotype.Repository;

import lombok.Data;

@Data
@Repository
public class HandoverProperties {
	private String dicomRootFolder;
	private String xlsFile;
	private String outputDir;
	private String sourceId;
	private String timestampCell;
	private String originalAccessionColumn;
	private Integer firstDataRow;
	private String falseAccessionColumn;
	private String handedColumn;
	
	/** Tag name to excel column letter. */
	private Map<String, String> customColumnMap;
}
