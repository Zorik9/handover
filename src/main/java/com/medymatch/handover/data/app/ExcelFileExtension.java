package com.medymatch.handover.data.app;

public enum ExcelFileExtension {
	xls, xlsx
}
