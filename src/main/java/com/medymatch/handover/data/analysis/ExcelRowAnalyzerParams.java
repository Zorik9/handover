package com.medymatch.handover.data.analysis;

import java.util.Map;

import com.medymatch.handover.dicom.ITagReplacementAlgorithm;
import com.medymatch.handover.dicom.analyzers.row.IRowCustomCellsUpdater;

import lombok.Data;

@Data
public class ExcelRowAnalyzerParams {
	private String inputDir;
	private String outputDir;
	private String sourceId;
	private String originalAccessionColumn;
	private String falseAccessionColumn;
	private String handedColumn;
	private String timestampCell;
	private ITagReplacementAlgorithm tagReplacementAlgorithm;
	private IRowCustomCellsUpdater rowCustomCellsUpdater;
	private Map<String, String> customColumnMap;
}
