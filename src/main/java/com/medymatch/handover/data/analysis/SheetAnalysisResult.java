package com.medymatch.handover.data.analysis;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class SheetAnalysisResult {	
	@Getter
	@Setter
	private List<String> analyzedDirs;
	
	@Getter
	private List<RowAnalysisResult> rowsAnalysisResult;
	@Getter
	private int numOfAnalyzedRows;
	@Getter
	private int numOfAlreadyAnalyzedRows;
	@Getter
	private int numOfInvalidRows;
	@Getter
	private int numOfEmptyResultRows;
	@Getter
	private int totalNumOfRows;
	
	public SheetAnalysisResult(){
		rowsAnalysisResult = new ArrayList<>();
	}
	
	public void addRowAnalysisResult(RowAnalysisResult rowAnalysisResult){
		rowsAnalysisResult.add(rowAnalysisResult);
		updateStatistics(rowAnalysisResult);
	}
	
	private void updateStatistics(RowAnalysisResult rowAnalysisResult){
		switch(rowAnalysisResult){
		case analyzed:
			numOfAnalyzedRows++;
			break;		
		case alreadyAnalyzed:
			numOfAlreadyAnalyzedRows++;
			break;
		case invalid:
			numOfInvalidRows++;
			break;
		case skipped:
			numOfEmptyResultRows++;
			break;
		}
		totalNumOfRows++;
	}
}
