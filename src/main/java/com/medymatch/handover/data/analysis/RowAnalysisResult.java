package com.medymatch.handover.data.analysis;

public enum RowAnalysisResult {
	analyzed, alreadyAnalyzed, invalid, skipped
}