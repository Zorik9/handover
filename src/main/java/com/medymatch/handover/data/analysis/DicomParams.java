package com.medymatch.handover.data.analysis;

import java.util.Map;

import lombok.Data;

@Data
public class DicomParams {
	private String inputDirPath;
	private String outputDirPath;
	private Map<Integer, String> tagsReplacement;
	private String configFilePath;
}
