package com.medymatch.handover.data.analysis;

import com.medymatch.handover.data.app.HandoverProperties;
import com.medymatch.handover.dicom.analyzers.row.ExcelRowAnalyzer;
import com.medymatch.handover.dicom.analyzers.sheet.IColumnsAutoSizer;
import com.medymatch.handover.dicom.analyzers.sheet.IHeadersUpdater;
import lombok.Data;
import org.apache.poi.ss.usermodel.Sheet;

@Data
public class ExcelSheetAnalyzerParams {
	private Sheet sheet;
	private HandoverProperties handoverProperties;
	private ExcelRowAnalyzer excelRowAnalyzer;
	private IColumnsAutoSizer columnsAutoSizer;
	private IHeadersUpdater headersUpdater;
}
