package com.medymatch.handover.data.analysis;

import lombok.Data;

@Data
public class StudyParams {
	private String accessionNumber;
	private String tagsPropertiesFile;
	private String sourceId;
}
