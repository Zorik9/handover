package com.medymatch.handover.data.analysis;

import org.apache.poi.ss.usermodel.Sheet;

import com.medymatch.handover.data.app.HandoverProperties;
import com.medymatch.handover.dicom.analyzers.row.ExcelRowAnalyzer;

import lombok.Data;

@Data
public class AnalyzerParams {
	private HandoverProperties handoverProperties;
	private Sheet sheet;
	private int firstDataRow;
	private ExcelRowAnalyzer excelRowAnalyzer;
}
