package com.medymatch.handover.utils.reflection;

public interface IMethodInvoker {
	public Object reflectionCall(final Object instance, final String className, final String methodName, final Class<?>[] parameterTypes, final Object[] parameters);
	
	public Object reflectionCall(final Object instance, final String className, final String amethodName, final Class<?> parameterType, final Object parameter);
}
