package com.medymatch.handover.utils.reflection;

import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;

import org.springframework.stereotype.Service;

@Service("MethodInvokerService")
public class MethodInvoker implements IMethodInvoker{
	
	public Object reflectionCall(final Object instance, final String className, final String methodName, final Class<?>[] parameterTypes, final Object[] parameters){
	    try{
	    	Class<?> cls = getClass(instance, className);
	    	
		    final Method method = cls.getMethod(methodName, parameterTypes);
		    
	    	setMethodAccessible(method);
	    	
		    return method.invoke(instance, parameters);
	    }
	    catch(Exception e){
	    	throw new RuntimeException(String.format("Failed to invoke the method %s", methodName), e);
	    }
	}

	private Class<?> getClass(final Object instance, final String className) throws ClassNotFoundException {
		Class<?> cls = instance == null ? Class.forName(className) : instance.getClass();
		return cls;
	}

	private void setMethodAccessible(final Method method) {
		AccessController.doPrivileged(new PrivilegedAction<Object>() {
			public Object run() {
		        method.setAccessible(true);
		        return null;
		    }
		});
	}
	
	public Object reflectionCall(final Object instance, final String className, final String amethodName, final Class<?> parameterType, final Object parameter){
		
		Object obj = getObjectInstance(parameterType, parameter);
		
		return reflectionCall(instance, className, amethodName, new Class[]{parameterType}, new Object[]{obj});
	}

	private Object getObjectInstance(final Class<?> parameterType, final Object parameter) {
		return parameterType.getName().equals("java.lang.String")?
				parameter :
				reflectionCall(null, parameterType.getName(), "valueOf", new Class[]{String.class}, new Object[]{parameter});
	}
}
