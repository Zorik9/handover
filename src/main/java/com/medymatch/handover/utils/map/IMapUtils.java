package com.medymatch.handover.utils.map;

import java.util.Map;
import java.util.Map.Entry;

public interface IMapUtils {
	public <K, V> Entry<K, V> getFirstEntry(Map<K, V> map);
}
