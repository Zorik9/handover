package com.medymatch.handover.utils.map;

import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Service;

@Service("MapUtilsService")
public class MapUtils implements IMapUtils{

	public <K, V> Entry<K, V> getFirstEntry(Map<K, V> map){
		if(map == null){
			return null;
		}
		return map.entrySet().iterator().next();
	}
}
