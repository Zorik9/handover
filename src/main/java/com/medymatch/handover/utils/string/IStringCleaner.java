package com.medymatch.handover.utils.string;

public interface IStringCleaner {
	public String cleanString(String str);
}
