package com.medymatch.handover.utils.string;

public interface IStringCapitilizer {
	public String capitilize(String str);
	
	public String deCapitilize(String str);
	
	public StringBuilder capitilizeProperty(String property, char separator);
}
