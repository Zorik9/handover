package com.medymatch.handover.utils.string;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service("StringCleanerService")
@Scope("prototype")
public class StringCleaner implements IStringCleaner {
	public String cleanString(String str){
		if(str == null){
			return null;
		}
		return str.trim().trim().replaceAll("\t", "");
	}
}
