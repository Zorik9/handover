package com.medymatch.handover.utils.string;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.medymatch.handover.functional_interfaces.Function;

@Service("StringCapitilizerService")
@Scope("prototype")
public class StringCapitilizer implements IStringCapitilizer{
	
	public String capitilize(String str){
		return performCapitalizationFunction(str, new Function<String, String>() {
			@Override
			public String apply(String string) {
				return string.substring(0, 1).toUpperCase();
			}
		});
	}
	
	public String deCapitilize(String str){
		return performCapitalizationFunction(str, new Function<String, String>() {
			@Override
			public String apply(String string) {
				return string.substring(0, 1).toLowerCase();
			}
		});
	}
	
	public StringBuilder capitilizeProperty(String property, char separator) {
		int indexOfSeparator = -1;
		int lastIndexOfSeparator = -1;
		StringBuilder destinationField = new StringBuilder();
		
		for(indexOfSeparator = property.indexOf(separator, indexOfSeparator + 1); indexOfSeparator != -1; lastIndexOfSeparator = indexOfSeparator, indexOfSeparator = property.indexOf(separator, indexOfSeparator + 1)){
			destinationField.append(property.substring(lastIndexOfSeparator + 1, lastIndexOfSeparator + 2).toUpperCase())
							.append(property.substring(lastIndexOfSeparator + 2, indexOfSeparator));
		}
		
		destinationField = capitilizeDestinationPropertyLastItem(property, destinationField, lastIndexOfSeparator);
		return destinationField;
	}

	private StringBuilder capitilizeDestinationPropertyLastItem(String property, StringBuilder destinationField,
			int lastIndexOfSeparator) {
		
		if(lastIndexOfSeparator == -1){
			destinationField = new StringBuilder(capitilize(property));
		}
		else{
			if(lastIndexOfSeparator < property.length()){
				destinationField.append(capitilize(property.substring(lastIndexOfSeparator + 1)));
			}
		}
		return destinationField;
	}
	
	private String performCapitalizationFunction(String str, Function<String, String> capitalizationFunc){
		if(StringUtils.isEmpty(str)){
			return null;
		}
		return capitalizationFunc.apply(str) + str.substring(1);
	}
}
