package com.medymatch.handover.utils.file;

import java.io.File;
import java.nio.file.Path;

import org.springframework.stereotype.Service;

@Service("DirectoryDeleterService")
public class DirectoryDeleter implements IDirectoryDeleter {
	public boolean deleteDirectory(Path path) {
		File deleteFile = path.toFile();
		boolean deleteSucceded = deleteDirectory(deleteFile);
		
		if(!deleteSucceded){
			return false;
		}
		
		if(deleteFile.exists()){
			return deleteFile.delete();
		}
		
		return true;
		
	}
	
	public boolean deleteDirectory(File file) {
	    if (file.exists()) {
	        File[] files = file.listFiles();
	        for (int i = 0; i < files.length; i++) {
	            if (files[i].isDirectory()) {
	                deleteDirectory(files[i]);
	            } else {
	                files[i].delete();
	            }
	        }
	    }
	    return (file.delete());
	}
}
