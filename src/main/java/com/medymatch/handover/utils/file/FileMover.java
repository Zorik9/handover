package com.medymatch.handover.utils.file;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

@Service("FileMoverService")
public class FileMover implements IFileMover {
	public void moveFile(File source, File destination, String destFileTitle){
		try{
			FileUtils.moveFile(source, destination);
		}
		catch (IOException ie) {
			throw new RuntimeException(String.format("Failed to override %s: %s", destFileTitle, destination), ie);
		}
	}
}
