package com.medymatch.handover.utils.file;

import java.io.File;

public interface IFilePathBuilder {
	public String getFilePath(File directory, String fileName);
	
	public String getFileName(String filePath);
	
	public String getParentFilePath(File inputFile);
	
	public String getParentFileName(File inputFile);
	
	public File getParentFile(File inputFile);
	
	public String getPath(String path);
}
