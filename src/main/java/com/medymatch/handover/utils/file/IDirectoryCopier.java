package com.medymatch.handover.utils.file;

import java.io.File;

public interface IDirectoryCopier {
	public void copyFileToTargetDir(File destDir, File file, String fileTitle);
	
	public void copyDirToDir(String srcDirPath, String destDirPath);
	
	public void copyDirToDir(File srcDir, File destDir);
	
	public void copyFileToDir(String srcFilePath, String destFilePath);
	
	public void copyFileToDir(File srcFile, File destFile);
}
