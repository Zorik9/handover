package com.medymatch.handover.utils.file;

import java.io.File;

public interface IFileCopier {
	public void copyFile(File srcFile, File destFile, String destFileTitle);
	
	public void copyFile(File srcFile, File destFile, boolean makeDistDirs, String destFileTitle);
}
