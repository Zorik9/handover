package com.medymatch.handover.utils.file;

public interface IConfigFileFetcher {
	public String fetchActualConfigFilePath(String configFilePath, String defaultFileName);
}
