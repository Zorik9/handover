package com.medymatch.handover.utils.file;

import java.io.File;
import java.nio.file.Path;

public interface IDirectoryDeleter {
	public boolean deleteDirectory(Path path);
	
	public boolean deleteDirectory(File file);
}
