package com.medymatch.handover.utils.file;

import java.io.File;

public interface IFileMover {
	public void moveFile(File source, File destination, String destFileTitle);
}
