package com.medymatch.handover.utils.file;

import java.io.File;
import java.nio.file.Paths;

import org.springframework.stereotype.Service;

@Service("FilePathBuilderService")
public class FilePathBuilder implements IFilePathBuilder {
	
	public String getFilePath(File directory, String fileName){
		return directory.getAbsolutePath() + File.separator + fileName;
	}
	
	public String getFileName(String filePath){
		File file = new File(filePath);
		return file.getName();
	}
	
	public String getParentFilePath(File inputFile){
		if(inputFile == null){
			return null;
		}
		File parentFile = getParentFile(inputFile);
		
		if(parentFile == null){
			return null;
		}
		
		return parentFile.getAbsolutePath();
	}
	
	public String getParentFileName(File inputFile){
		if(inputFile == null){
			return null;
		}
		File parentFile = getParentFile(inputFile);
		
		if(parentFile == null){
			return null;
		}
		
		return parentFile.getName();
	}
	
	public File getParentFile(File inputFile){
		if(inputFile == null){
			return null;
		}
		
		File parentFile = inputFile.getParentFile();
		
		if(parentFile != null){
			return parentFile;
		}
		
		String fullPath = inputFile.getAbsolutePath();
		File file = new File(fullPath);
		return file.getParentFile();
	}
	
	public String getPath(String path){
		if(path == null){
			return null;
		}
		
		path = path.replace("\\", "\\\\");
		return Paths.get(path).toString();
	}
}
