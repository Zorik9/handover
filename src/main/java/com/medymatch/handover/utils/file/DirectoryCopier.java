package com.medymatch.handover.utils.file;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service("DirectoryCopierService")
public class DirectoryCopier implements IDirectoryCopier {
	
	@Autowired
	private IFilePathBuilder filePathBuilder;
	
	@Autowired
	private IFileCopier fileCopier;

	public void copyFileToTargetDir(File destDir, File file, String fileTitle) {
		File destFile;
		File destTargetDir;
		String destTargetDirPath;
		String destFilePath;
		
		File dicomFolder = filePathBuilder.getParentFile(file);
		
		if(dicomFolder == null){
			destTargetDirPath = destDir.getAbsolutePath();
			destFilePath = filePathBuilder.getFilePath(destDir, file.getName());
		}
		
		destTargetDirPath = filePathBuilder.getFilePath(destDir, dicomFolder.getName());
		destTargetDir = new File(destTargetDirPath);
		destFilePath = filePathBuilder.getFilePath(destTargetDir, file.getName());
		destFile = new File(destFilePath);
		
		fileCopier.copyFile(file, destFile, fileTitle);
	}
	
	public void copyDirToDir(String srcDirPath, String destDirPath) {
		copyDirToDir(new File(srcDirPath), new File(destDirPath));
	}
	public void copyDirToDir(File srcDir, File destDir) {
		try {
			FileUtils.copyDirectory(srcDir, destDir, false);
		} catch (IOException e) {
			throw new RuntimeException(String.format("Failed to copy the input directory %s to the output directory %s.", srcDir.toString(), destDir.toString()), e);
		}
	}
	
	public void copyFileToDir(String srcFilePath, String destFilePath){
		copyFileToDir(new File(srcFilePath), new File(destFilePath));
	}
	
	public void copyFileToDir(File srcFile, File destFile){
		try {
			FileUtils.copyFileToDirectory(srcFile, destFile, false);
		} catch (IOException e) {
			throw new RuntimeException(String.format("Failed to copy the input file %s to the target directory %s.", srcFile, destFile), e);
		}
	}
}
