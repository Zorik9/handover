package com.medymatch.handover.utils.file;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service("ConfigFileFetcherService")
public class ConfigFileFetcher implements IConfigFileFetcher{
	
	private static Logger log = Logger.getLogger(ConfigFileFetcher.class);
	
	@Autowired
	private IProjectRootFetcher projectRootFetcher;
	
	public String fetchActualConfigFilePath(String configFilePath, String defaultFileName){
		
		if(!StringUtils.isEmpty(configFilePath)){
			return configFilePath;
		}

		log.info("Input config file parameter is empty. Attemting to fetch the config file from working directory.");

		File defaultAppProperties = new File(defaultFileName);
		if (defaultAppProperties.exists()) {
			return defaultFileName;
		}

		log.info("Could not find config file.");
		return null;
	}

	private Path getPathFromProjectRoot(File projectRoot, String defaultFileName) {
		return Paths.get(projectRoot.getAbsolutePath() +
					File.separator + defaultFileName);
	}

	private Path getPathFromResourcesDir(File projectRoot, String defaultFileName) {
		return Paths.get(projectRoot.getAbsolutePath() +
				File.separator + "src" + File.separator +
				"main" + File.separator + "resources" +
				File.separator + defaultFileName);
	}
}
