package com.medymatch.handover.utils.file;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service("ProjectRootFetcherService")
public class ProjectRootFetcher implements IProjectRootFetcher{
	private static Logger log = Logger.getLogger(ProjectRootFetcher.class);
	
	public File getProjectRoot(){
		return new File(getProjectRootPath().toUri());
	}
	
	public Path getProjectRootPath(){
		
		log.info("Searching for the root path of the project. The search starting point is the current class.");
        
		String className = getClass().getName();
		
		URL classfileResource = getClassfileResource(className);
        
        Path absolutePackagePath = getAbsolutePathPackage(classfileResource);
		
        return getRootPath(className, absolutePackagePath);
	}
	
	private URL getClassfileResource(String className){
		log.debug("Current class name: " + className);
        String classfileName = "/" + className.replace('.', '/') + ".class";
        URL classfileResource = getClass().getResource(classfileName);
        
        if(classfileResource == null){
        	throw new RuntimeException("Failed to find the class file resource. Therefore, the root path of the project cannot be found.");
        }
        
        return classfileResource;
	}

	private Path getAbsolutePathPackage(URL classfileResource) {
		Path absolutePackagePath = null;
		try {
			String resourcePath = getResourcePath(classfileResource);
			URI resourceUri = new URI(resourcePath);
			log.debug("current class file resource uri path: " + Paths.get(resourceUri));
			absolutePackagePath = Paths.get(resourceUri).getParent();
		} catch (URISyntaxException e) {
			throw new RuntimeException("Failed to fetch project root path, due to URI syntax exception", e);
		}
		return absolutePackagePath;
	}

	private String getResourcePath(URL classfileResource) throws URISyntaxException {
		String resourcePath = classfileResource.toURI().toString();
		log.debug("current class file resource path is: " + resourcePath);
		
		if(resourcePath.contains("jar:")){
			resourcePath = resourcePath.replace("jar:", "").
						   replaceFirst("tarball-copier-.*.jar!", "classes");
			
			log.debug("Updated resource path: " + resourcePath);
			
		}
		return resourcePath;
	}
	
	private Path getRootPath(String className, Path absolutePackagePath) {
		int packagePathSegments = className.length() - className.replace(".", "").length();
        // Remove package segments from path, plus two more levels
        // for "target/classes", which is the standard location for
        // classes in Eclipse.
        Path path = absolutePackagePath;
        for (int i = 0, segmentsToRemove = packagePathSegments + 2;
                i < segmentsToRemove; i++) {
            path = path.getParent();
        }
        
        return path;
	}
}
