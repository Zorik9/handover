package com.medymatch.handover.utils.file;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("FileCopierService")
public class FileCopier implements  IFileCopier{
	
	@Autowired
	private IFilePathBuilder filePathBuilder;
	
	public void copyFile(File srcFile, File destFile, String destFileTitle){
		copyFile(srcFile, destFile, true, destFileTitle);
	}
	
	public void copyFile(File srcFile, File destFile, boolean makeDistDirs, String destFileTitle){
		validateFileCopierInput(srcFile, destFile);
		
		if(makeDistDirs){
			makeDirs(destFile);
		}
		
		copySrcFileToDestFile(srcFile, destFile, destFileTitle);
	}

	private void makeDirs(File destFile) {
		File parentFile = filePathBuilder.getParentFile(destFile);
		if(parentFile != null){
			parentFile.mkdirs();
		}
	}
	
	private void copySrcFileToDestFile(File srcFile, File destFile, String destFileTitle){
		try{
			FileUtils.copyFile(srcFile, destFile);
		}
		catch (IOException ie) {
			throw new RuntimeException(String.format("Failed to copy the %s: %s, due to IO issues.", destFileTitle, destFile), ie);
		}
	}
	
	private void validateFileCopierInput(File srcFile, File destFile){
		validateNotEmpty(srcFile, "source file");
		validateNotEmpty(destFile, "destination file");
	}
	
	private void validateNotEmpty(File file, String title){
		if(file == null){
			throw new RuntimeException(String.format("Could not copy the %s, because it's empty.", title));
			
		}
	}
}
