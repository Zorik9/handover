package com.medymatch.handover.utils.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.medymatch.handover.utils.string.IStringCleaner;

@Service("CellUtilsService")
public class CellUtils implements ICellUtils{
	
	@Autowired
	private IStringCleaner stringCleaner;
	
	public void updateCellValue(Row row, String columnLetter, String cellValue){
		Cell cell = getCell(row, columnLetter, true);
		updateCellValue(cell, cellValue);
	}
	
	public void updateCellValue(Cell cell, String cellValue){
		if(cell == null){
			return;
		}
		cell.setCellType(Cell.CELL_TYPE_STRING);
		cell.setCellValue(stringCleaner.cleanString(cellValue));
	}
	
	public Cell getCell(Sheet sheet, String cellAddress){
		CellReference ref = new CellReference(cellAddress);
		Row row = sheet.getRow(ref.getRow());
		if(row == null){
			return null;
		}
	    return getCell(row, ref.getCol(), true);
	}
	
	public String getCellValue(Row row, String columnLetter, boolean createIfNotExist){
		Cell cell = getCell(row, columnLetter, createIfNotExist);
		return getCellValue(cell);
	}
	
	public String getCellValue(Cell cell){
		if(cell == null){
			return null;
		}
		DataFormatter dataFormatter = new DataFormatter();
		return stringCleaner.cleanString(dataFormatter.formatCellValue(cell));	
	}
	
	public Cell getCell(Row row, int columnIndex, boolean createIfNotExist){
		return getCell(row, CellReference.convertNumToColString(columnIndex), createIfNotExist);
	}

	public Cell getCell(Row row, String columnLetter, boolean createIfNotExist){
		int columnIndex = getColumnIndex(columnLetter);
		Cell cell = row.getCell(columnIndex);
		
		if(cell != null){
			return cell;
		}
		
		return createIfNotExist ? row.createCell(columnIndex) : null;
	}
	
	public boolean isEmptyCell(Cell cell){
		if(cell == null){
			return true;
		}
		
		return StringUtils.isEmpty(getCellValue(cell));
	}
	
	public int getColumnIndex(String columnLetter){
		return CellReference.convertColStringToIndex(columnLetter);
	}
}
