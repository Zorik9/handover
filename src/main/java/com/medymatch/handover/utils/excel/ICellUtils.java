package com.medymatch.handover.utils.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public interface ICellUtils {
	
	public void updateCellValue(Row row, String columnLetter, String cellValue);
	
	public void updateCellValue(Cell cell, String cellValue);
	
	public Cell getCell(Sheet sheet, String cellAddress);
	
	public String getCellValue(Row row, String columnLetter, boolean createIfNotExist);
	
	public String getCellValue(Cell cell);
	
	public Cell getCell(Row row, String columnLetter, boolean createIfNotExist);
	
	public boolean isEmptyCell(Cell cell);
	
	public int getColumnIndex(String columnLetter);
}
