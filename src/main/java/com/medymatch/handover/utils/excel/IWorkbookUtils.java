package com.medymatch.handover.utils.excel;

import org.apache.poi.ss.usermodel.Workbook;

public interface IWorkbookUtils {
	public void closeWorkbook(Workbook workbook);
	
	public void writeToWorkbook(String xlsFilePath, Workbook workbook);
}
