package com.medymatch.handover.utils.excel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

@Service("WorkbookUtilsService")
public class WorkbookUtils implements IWorkbookUtils{
	public void closeWorkbook(Workbook workbook) {
		try {
			workbook.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void writeToWorkbook(String xlsFilePath, Workbook workbook) {
		FileOutputStream out;
		try {
			out = new FileOutputStream(new File(xlsFilePath));
			workbook.write(out);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
