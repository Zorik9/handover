package com.medymatch.handover.utils.excel;

import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;

import com.medymatch.handover.data.app.HandoverProperties;

public interface IColumnUtils {
	public List<String> extractColumnsLetters(HandoverProperties handoverProperties);
	
	public List<Integer> extractColumns(HandoverProperties handoverProperties);
	
	public void autoSizeColumn(Sheet sheet, String columnLetter);
}
