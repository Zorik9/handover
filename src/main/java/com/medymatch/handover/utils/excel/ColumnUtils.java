package com.medymatch.handover.utils.excel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.medymatch.handover.data.app.HandoverProperties;

@Service("ColumnUtilsService")
public class ColumnUtils implements IColumnUtils {
	
	@Autowired
	ICellUtils cellUtils;

	public List<String> extractColumnsLetters(HandoverProperties handoverProperties){
		
		if(handoverProperties == null){
			return null;
		}
		
		List<String> columnsLetters = new ArrayList<>();
		columnsLetters.addAll(getGeneralColumnLettrs(handoverProperties));
		columnsLetters.addAll(getCustomColumnsLetters(handoverProperties.getCustomColumnMap()));
		return columnsLetters;
	}
	
	private List<String> getGeneralColumnLettrs(HandoverProperties handoverProperties){
		List<String> generalColumnsLetters = new ArrayList<>();
		generalColumnsLetters.add(handoverProperties.getOriginalAccessionColumn());
		generalColumnsLetters.add(handoverProperties.getFalseAccessionColumn());
		generalColumnsLetters.add(handoverProperties.getHandedColumn());
		return generalColumnsLetters;
	}

	private List<String> getCustomColumnsLetters(Map<String, String> customColumnMap) {
		List<String> customColumnsLetters = new ArrayList<>();
		if(customColumnMap != null && customColumnMap.size() > 0){
			for (String tagName : customColumnMap.keySet()) {
				customColumnsLetters.add(customColumnMap.get(tagName));
			}
		}
		return customColumnsLetters;
	}
	
	public List<Integer> extractColumns(HandoverProperties handoverProperties){
		List<String> columnsLetters = extractColumnsLetters(handoverProperties);
		
		if(columnsLetters == null){
			return null;
		}
		
		List<Integer> columns = new ArrayList<>();
		for (String columnLetter : columnsLetters) {
			columns.add(cellUtils.getColumnIndex(columnLetter));
		}
		
		return columns;
	}
	
	public void autoSizeColumn(Sheet sheet, String columnLetter) {
		int columnIndex = CellReference.convertColStringToIndex(columnLetter);
		sheet.autoSizeColumn(columnIndex);
	}
}
