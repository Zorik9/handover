package com.medymatch.handover.utils.excel;

import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("RowUtilsService")
public class RowUtils implements IRowUtils {
	
	@Autowired
	private ICellUtils cellUtils;
	
	public boolean isEmptyRow(Row row, List<Integer> columns){
		
		if(columns == null || row == null){
			return true;
		}
		
		int countEmptyCells = 0;
		for (Integer column : columns) {
			if(cellUtils.isEmptyCell(row.getCell(column))){
				countEmptyCells++;
			}
		}
		
		return columns.size() == countEmptyCells;
	}
}
