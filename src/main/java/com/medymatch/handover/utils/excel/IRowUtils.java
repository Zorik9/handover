package com.medymatch.handover.utils.excel;

import java.util.List;

import org.apache.poi.ss.usermodel.Row;

public interface IRowUtils {
	public boolean isEmptyRow(Row row, List<Integer> columns);
}
