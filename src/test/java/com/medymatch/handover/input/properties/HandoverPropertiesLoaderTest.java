package com.medymatch.handover.input.properties;

import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.medymatch.handover.config.AppConfig;
import com.medymatch.handover.data.app.HandoverProperties;

@Service
@ContextConfiguration(classes = AppConfig.class, loader = AnnotationConfigContextLoader.class)
public class HandoverPropertiesLoaderTest extends AbstractTestNGSpringContextTests{
	private static Logger log = Logger.getLogger(HandoverPropertiesLoaderTest.class);
	
	private AbstractApplicationContext context;
	
	@Autowired
	IHandoverPropertiesLoader handoverPropertiesLoader;
	
	@BeforeMethod
	private void initContex(){
		log.info("Creating a context instance for Handover project.");
		context = new AnnotationConfigApplicationContext(AppConfig.class);
	}
	
	@AfterMethod
	private void finalizeContext(){
		Assert.assertTrue(closeContext());
	}
	
	@Test
	public void testGetHandoverProperties(){
		Properties properties = new PropertiesLoader().readPropertiesFromResourceFolder("app.properties");
		HandoverProperties handoverProperties = handoverPropertiesLoader.getHandoverProperties(properties);
		Assert.assertNotNull(handoverProperties);
		
		String propertyKey;
		String propertyValue;
		
		for (Iterator<Object> it = properties.keySet().iterator(); it.hasNext();) {
			propertyKey = (String) it.next();
			propertyValue = properties.getProperty(propertyKey);
			
			Assert.assertNotNull(propertyValue);
		}
	}
	
	private boolean closeContext(){
		if(context != null){
			context.close();
			log.info("Spring context has been successfully closed.");
			return true;
		}
		else{
			log.warn("Failed to close spring context, because it's already empty.");
			return false;
		}
	}
}
