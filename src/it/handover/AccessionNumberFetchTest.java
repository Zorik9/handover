package handover;

import org.dcm4che2.data.DicomObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import pl.psnc.scape.dicom.io.reader.DicomReader;
import pl.psnc.scape.dicom.tag.TagFetcher;

public class AccessionNumberFetchTest {
	@Test
	public void testReadDicomData(){
		String dicomFilePath = "C:\\Users\\shahar\\Customers\\MedyMatch\\DicomFiles\\Batch 3 Part 1\\_____________20160905_134715960\\CT.1.2.840.114356.2016.8.5.13.49.58.19.2.2.1.dcm";
		String accessionNumber = getAccessionNumberFromDicomFile(dicomFilePath);
		Assert.assertNotNull(accessionNumber);
	}
	
	public String getAccessionNumberFromDicomFile(String dicomFilePath){
		DicomReader dicomReader = new DicomReader();
		DicomObject dicomObject = dicomReader.read(dicomFilePath);
		String tagKey = "(0008,0050)";
		int tagNumber = new TagFetcher().getTag(tagKey);
		return dicomObject.getString(tagNumber);
	}
}
