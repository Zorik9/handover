package runner;

import com.medymatch.handover.main_args.MainArguments;
import com.medymatch.handover.main_args.MainArgumentsManager;

public class TestRunner {
	private MainArguments mainArguments;
	
	public TestRunner(MainArguments mainArguments) {
		this.mainArguments = mainArguments;
	}

	public boolean runTest(){
		return runTransaction();
	}

	private boolean runTransaction(){
		System.out.println("Running Handover Tool in integration test mode...");
		MainArgumentsManager mainArgumentsManager = new MainArgumentsManager();
		return mainArgumentsManager.doTransaction(mainArguments);
	}
}
