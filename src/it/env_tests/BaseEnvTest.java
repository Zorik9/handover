package env_tests;

import org.testng.Assert;

import com.medymatch.handover.main_args.MainArguments;

import runner.TestRunner;

public class BaseEnvTest {
	protected MainArguments mainArguments;
	
	protected String appPropertiesFile;
	
	protected TestRunner testRunner;
	
	public void runTest(){
		setMainArguments();
		testRunner = new TestRunner(mainArguments);
		Assert.assertTrue(testRunner.runTest());
	}
	
	protected void setMainArguments(){
		System.out.println("Setting data for integration test.\n");
		mainArguments = new MainArguments();
		mainArguments.setAppPropertiesFile(appPropertiesFile);
	}
}
