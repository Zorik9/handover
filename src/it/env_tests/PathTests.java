package env_tests;

import org.testng.annotations.Test;

public class PathTests extends BaseEnvTest{
	@Test
	public void testFullPath(){
		appPropertiesFile = "src\\test\\resources\\dicom-env-tests\\path-tests\\full-path\\app.properties";
		runTest();
	}
	
	@Test
	public void testShortPath(){
		appPropertiesFile = "src\\test\\resources\\dicom-env-tests\\path-tests\\short-path\\app.properties";
		runTest();
	}
	
	@Test
	public void testMinimalPath(){
		appPropertiesFile = "src\\test\\resources\\dicom-env-tests\\path-tests\\minimal-path\\app.properties";
		runTest();
	}
	
	@Test
	public void testFullNonJavaPath(){
		appPropertiesFile = "src\\test\\resources\\dicom-env-tests\\path-tests\\full-non-java-path\\app.properties";
		runTest();
	}
}
